package com.gitee.sop.gateway.service.impl;

import com.gitee.sop.gateway.common.ApiInfoDTO;
import com.gitee.sop.gateway.common.enums.YesOrNoEnum;
import com.gitee.sop.gateway.config.ApiConfig;
import com.gitee.sop.gateway.request.ApiRequestContext;
import com.gitee.sop.gateway.response.ApiResponse;
import com.gitee.sop.gateway.response.NoCommonResponse;
import com.gitee.sop.gateway.response.Response;
import com.gitee.sop.gateway.service.ResultWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 六如
 */
@Service
public class ResultWrapperImpl implements ResultWrapper {

    @Autowired
    private ApiConfig apiConfig;

    @Override
    public Response wrap(ApiRequestContext context, ApiInfoDTO apiInfoDTO, Object result) {
        Integer hasCommonResponse = apiInfoDTO == null ?
                YesOrNoEnum.YES.getValue()
                : apiInfoDTO.getHasCommonResponse();
        boolean needNotWrap = YesOrNoEnum.of(hasCommonResponse) == YesOrNoEnum.NO;
        if (result instanceof ApiResponse) {
            ApiResponse apiResponse = (ApiResponse) result;
            return executeApiResponse(apiResponse, needNotWrap);
        }
        // 不需要公共返回参数
        if (needNotWrap) {
            return NoCommonResponse.success(result);
        }
        return ApiResponse.success(result);
    }

    private Response executeApiResponse(ApiResponse apiResponse, boolean needNotWrap) {
        // 不需要公共返回参数
        if (needNotWrap) {
            return NoCommonResponse.success(apiResponse.getData());
        }
        return apiResponse;
    }

}
