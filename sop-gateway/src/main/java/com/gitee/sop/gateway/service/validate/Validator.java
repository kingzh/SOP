package com.gitee.sop.gateway.service.validate;

import com.gitee.sop.gateway.common.ApiInfoDTO;
import com.gitee.sop.gateway.request.ApiRequestContext;

/**
 * 校验接口
 *
 * @author 六如
 */
public interface Validator {
    /**
     * 接口验证
     *
     * @param apiRequestContext 请求内容
     * @return 校验通过返回路由信息
     */
    ApiInfoDTO validate(ApiRequestContext apiRequestContext);

}
