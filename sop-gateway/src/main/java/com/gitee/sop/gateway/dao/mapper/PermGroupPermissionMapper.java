package com.gitee.sop.gateway.dao.mapper;

import com.gitee.fastmybatis.core.mapper.BaseMapper;
import com.gitee.sop.gateway.dao.entity.PermGroupPermission;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 六如
 */
@Mapper
public interface PermGroupPermissionMapper extends BaseMapper<PermGroupPermission> {

}
