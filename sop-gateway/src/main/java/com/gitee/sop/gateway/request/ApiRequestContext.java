package com.gitee.sop.gateway.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Locale;

/**
 * @author 六如
 */
@Builder
@Getter
@Setter
public class ApiRequestContext {
    /**
     * 请求参数
     */
    private ApiRequest apiRequest;
    /**
     * 本地语言
     */
    private Locale locale;
    /**
     * 客户端ip
     */
    private String ip;
    /**
     * traceId
     */
    private String traceId;
    /**
     * 上传文件
     */
    private UploadContext uploadContext;
}
