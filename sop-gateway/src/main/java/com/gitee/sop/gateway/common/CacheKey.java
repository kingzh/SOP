package com.gitee.sop.gateway.common;

/**
 * @author 六如
 */
public class CacheKey {
    public static final String KEY_API = "sop:api";
    public static final String KEY_ISV = "sop:isv";
    public static final String KEY_SEC = "sop:sec";
    public static final String KEY_ISV_PERM = "sop:isv-perm";
}
