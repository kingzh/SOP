package com.gitee.sop.gateway.dao.entity;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * 表名：isv_info
 * 备注：isv信息表
 *
 * @author 六如
 */
@Table(name = "isv_info", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
@Data
public class IsvInfo {

    private Long id;

    /**
     * appKey
     */
    private String appId;

    /**
     * 1启用，2禁用
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    private LocalDateTime addTime;

    private LocalDateTime updateTime;


}
