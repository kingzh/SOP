package com.gitee.sop.gateway.common;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @author 六如
 */
public final class SopConstants {

    private SopConstants() {
    }

    public static final Charset CHARSET_UTF8 = StandardCharsets.UTF_8;
    public static final String UTF8 = "UTF-8";
    public static final String NULL = "null";

}
