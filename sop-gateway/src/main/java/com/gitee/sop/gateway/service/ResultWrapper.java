package com.gitee.sop.gateway.service;

import com.gitee.sop.gateway.common.ApiInfoDTO;
import com.gitee.sop.gateway.request.ApiRequestContext;
import com.gitee.sop.gateway.response.ApiResponse;
import com.gitee.sop.gateway.response.Response;

/**
 * 结果包裹
 *
 * @author 六如
 */
public interface ResultWrapper {

    Response wrap(ApiRequestContext context, ApiInfoDTO apiInfoDTO, Object result);

    default Response wrap(ApiRequestContext context, ApiResponse apiResponse) {
        return wrap(context, null, apiResponse);
    }

}
