package com.gitee.sop.gateway.service.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONWriter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.gitee.sop.gateway.config.ApiConfig;
import com.gitee.sop.gateway.service.Serde;
import com.gitee.sop.gateway.util.XmlUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * @author 六如
 */
public class SerdeImpl implements Serde {

    static JSONWriter.Context writeContext;

    @Autowired
    protected ApiConfig apiConfig;

    @Value("${gateway.serialize.date-format}")
    protected String dateFormat;

    @Override
    public String toJson(Object object) {
        return JSON.toJSONString(object, writeContext);
    }

    @Override
    public String toXml(Object object) {
        try {
            return XmlUtil.toXml(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Map<String, Object> parseJson(String json) {
        return JSON.parseObject(json);
    }

    @PostConstruct
    public void init() {
        writeContext = new JSONWriter.Context();
        writeContext.setDateFormat(dateFormat);

        this.doInit();
    }

    protected void doInit() {

    }
}
