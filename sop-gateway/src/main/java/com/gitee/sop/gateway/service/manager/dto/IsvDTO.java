package com.gitee.sop.gateway.service.manager.dto;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class IsvDTO {

    private Long id;

    private String appId;

    private Integer status;

}
