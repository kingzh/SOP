package com.gitee.sop.gateway.common;

import com.gitee.sop.gateway.request.ApiRequestContext;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author 六如
 */
@Getter
@AllArgsConstructor
public class RouteContext {

    private ApiRequestContext apiRequestContext;
    private ApiInfoDTO apiInfo;

}
