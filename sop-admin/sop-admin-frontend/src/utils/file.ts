/**
 * 下载文件
 *
 * @param filename 文件名称,带后缀,如:aa.txt
 * @param text 文件内容
 */
export function downloadText(filename, text) {
  const element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," + encodeURIComponent(text)
  );
  element.setAttribute("download", filename);

  element.style.display = "none";
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}
