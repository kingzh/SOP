/**
 * 按钮权限定义
 */
export const PermCode = {
  // 接口管理按钮权限
  api: {
    // 更新
    update: "api:table-btn:update",
    // 启用/禁用
    updateStatus: "api:table-btn:updateStatus"
  },
  // 文档列表按钮定义
  doc: {
    // 发布
    publish: "doc:table-btn:publish",
    // 下线
    offline: "doc:table-btn:offline",
    // 同步
    sync: "doc:table-btn:sync",
    // 同步所有接口
    syncAll: "doc:page-btn:sync"
  }
};
