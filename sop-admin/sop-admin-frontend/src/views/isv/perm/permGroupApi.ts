import { ref } from "vue";
import {
  type ButtonsCallBackParams,
  type PageInfo,
  type PlusColumn,
  useTable
} from "plus-pro-components";
import { withInstall } from "@pureadmin/utils";
import permGroupApi from "./permGroupApi.vue";
import { StatusEnum } from "@/model/enums";
import { api as permApi, api } from "@/api/permGroupPermission";
import {
  handleClose,
  handleSaveApi,
  openDlg,
  searchFormData as apiSearFormData
} from "@/components/ApiSelect";
import { ElMessage } from "element-plus";

// ========= search form =========

// 查询表单对象
export const searchFormData = ref({
  groupId: 0,
  apiName: "",
  status: "",
  pageIndex: 1,
  pageSize: 10
});

// 查询表单字段定义
export const searchFormColumns: PlusColumn[] = [
  {
    label: "接口名称",
    prop: "apiName"
  },
  {
    label: "状态",
    prop: "status",
    width: 80,
    valueType: "select",
    options: [
      {
        label: "启用",
        value: StatusEnum.ENABLE,
        color: "green"
      },
      {
        label: "禁用",
        value: StatusEnum.DISABLE,
        color: "red"
      }
    ]
  }
];

// ========= table =========

handleSaveApi.value = apiIdList => {
  const groupId = searchFormData.value.groupId;
  if (groupId > 0) {
    permApi.setting(groupId, apiIdList).then(() => {
      ElMessage.success("保存成功");
      handleClose();
      search();
    });
  }
};

// 表格对象
export const {
  tableData,
  total,
  pageInfo,
  buttons: actionButtons
} = useTable<any[]>();
// 默认每页条数,默认10
pageInfo.value.pageSize = 10;
// 表格字段定义
export const tableColumns: PlusColumn[] = [
  {
    label: "所属应用",
    prop: "application",
    tableColumnProps: {
      showOverflowTooltip: true
    }
  },
  {
    label: "接口名称",
    prop: "apiName",
    tableColumnProps: {
      showOverflowTooltip: true
    }
  },
  {
    label: "版本号",
    prop: "apiVersion",
    width: 80
  },
  {
    label: "接口描述",
    prop: "description",
    tableColumnProps: {
      showOverflowTooltip: true
    }
  },
  {
    label: "注册来源",
    prop: "regSource",
    width: 100,
    valueType: "select",
    options: [
      {
        label: "系统",
        value: 1,
        color: "blue"
      },
      {
        label: "手动",
        value: 2,
        color: "green"
      }
    ]
  },
  {
    label: "状态",
    prop: "status",
    width: 80,
    valueType: "select",
    options: [
      {
        label: "启用",
        value: StatusEnum.ENABLE,
        color: "green"
      },
      {
        label: "禁用",
        value: StatusEnum.DISABLE,
        color: "red"
      }
    ]
  }
];
// 表格按钮定义
actionButtons.value = [
  {
    // 删除
    text: "删除",
    code: "delete",
    props: {
      type: "danger"
    },
    confirm: {
      options: { draggable: false }
    },
    onConfirm(params: ButtonsCallBackParams) {
      const data = {
        groupId: searchFormData.value.groupId,
        apiIdList: [params.row.id]
      };
      api.del(data).then(() => {
        ElMessage.success("删除成功");
        search();
      });
    }
  }
];

export const handleOpenDlg = () => {
  apiSearFormData.value.isPermission = 1;
  openDlg();
};

export const searchTable = groupId => {
  searchFormData.value.groupId = groupId;
  search();
};

// 点击查询按钮
export const handleSearch = () => {
  search();
};

// 分页事件
export const handlePaginationChange = (_pageInfo: PageInfo): void => {
  pageInfo.value = _pageInfo;
  search();
};

// 查询
export const search = async () => {
  try {
    const { data } = await doSearch();
    tableData.value = data.list;
    total.value = data.total;
  } catch (error) {}
};
// 请求接口
const doSearch = async () => {
  // 查询参数
  const data = searchFormData.value;
  // 添加分页参数
  data.pageIndex = pageInfo.value.page;
  data.pageSize = pageInfo.value.pageSize;

  return api.page(data);
};

const PermGroupApi = withInstall(permGroupApi);

export { PermGroupApi };
