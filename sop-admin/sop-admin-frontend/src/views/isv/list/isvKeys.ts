import { ref } from "vue";
import type { PlusFormGroupRow } from "plus-pro-components";
import { api } from "@/api/isvList";
import { KeyFormatEnum } from "@/model/enums";
import { ElMessage } from "element-plus";
import { useIsvList } from "./index";

const { search } = useIsvList();

// 弹窗显示
export const dlgKeysSetting = ref(false);
export const settingKeysFormData = ref<any>({
  isvId: 0,
  keyFormat: KeyFormatEnum.PKCS8,
  publicKeyIsv: "",
  privateKeyIsv: "",
  publicKeyPlatform: "",
  privateKeyPlatform: ""
});
export const settingKeysFormRules = {
  publicKeyIsv: [{ required: true, message: "不能为空" }],
  privateKeyIsv: [{ required: true, message: "不能为空" }],
  publicKeyPlatform: [{ required: true, message: "不能为空" }],
  privateKeyPlatform: [{ required: true, message: "不能为空" }]
};
// 表单内容
export const settingKeysFormColumns: PlusFormGroupRow[] = [
  {
    title: "基本信息",
    columns: [
      {
        label: "秘钥格式",
        prop: "keyFormat",
        labelWidth: 100,
        valueType: "radio",
        options: [
          {
            label: "PKCS8(Java适用)",
            value: KeyFormatEnum.PKCS8
          },
          {
            label: "PKCS1(非Java适用)",
            value: KeyFormatEnum.PKCS1
          }
        ]
      }
    ]
  },
  {
    title: "ISV公私钥",
    columns: [
      {
        label: "ISV公钥",
        prop: "publicKeyIsv",
        valueType: "textarea",
        labelWidth: 100,
        fieldProps: {
          showWordLimit: false,
          placeholder: "",
          readonly: true,
          autosize: { minRows: 2, maxRows: 4 }
        }
      },
      {
        label: "☆ISV私钥",
        prop: "privateKeyIsv",
        valueType: "textarea",
        labelWidth: 100,
        fieldProps: {
          showWordLimit: false,
          placeholder: "",
          autosize: { minRows: 2, maxRows: 4 }
        }
      },
      {
        labelWidth: 100,
        prop: "restIsvKeys",
        renderLabel: () => {
          return "";
        }
      }
    ]
  },
  {
    title: "平台公私钥",
    columns: [
      {
        label: "☆平台公钥",
        prop: "publicKeyPlatform",
        valueType: "textarea",
        labelWidth: 100,
        fieldProps: {
          showWordLimit: false,
          placeholder: "",
          autosize: { minRows: 2, maxRows: 4 }
        }
      },
      {
        label: "平台私钥",
        prop: "privateKeyPlatform",
        valueType: "textarea",
        labelWidth: 100,
        fieldProps: {
          showWordLimit: false,
          placeholder: "",
          autosize: { minRows: 2, maxRows: 4 }
        }
      },
      {
        labelWidth: 100,
        prop: "restPlatformKeys",
        renderLabel: () => {
          return "";
        }
      }
    ]
  }
];

export const settingKeys = (row: any) => {
  const params = {
    isvId: row.id
  };
  api.viewKeys(params).then(resp => {
    settingKeysFormData.value = resp.data;
    dlgKeysSetting.value = true;
  });
};

export const restIsvKeysLoading = ref(false);
export const resetIsvKeys = () => {
  restIsvKeysLoading.value = true;
  api.createKeys(settingKeysFormData.value.keyFormat).then(resp => {
    restIsvKeysLoading.value = false;
    const data = resp.data;
    settingKeysFormData.value.publicKeyIsv = data.publicKey;
    settingKeysFormData.value.privateKeyIsv = data.privateKey;
  });
};

export const resetPlatformKeysLoading = ref(false);
export const resetPlatformKeys = () => {
  resetPlatformKeysLoading.value = true;
  api.createKeys(settingKeysFormData.value.keyFormat).then(resp => {
    resetPlatformKeysLoading.value = false;
    const data = resp.data;
    settingKeysFormData.value.publicKeyPlatform = data.publicKey;
    settingKeysFormData.value.privateKeyPlatform = data.privateKey;
  });
};

export const handleUpdateKeys = () => {
  api.updateKeys(settingKeysFormData.value).then(() => {
    ElMessage.success("保存成功");
    dlgKeysSetting.value = false;
    search();
  });
};
