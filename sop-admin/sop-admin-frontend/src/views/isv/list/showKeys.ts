import { ref } from "vue";
import type { PlusFormGroupRow } from "plus-pro-components";
import { api } from "@/api/isvList";
import { KeyFormatEnum } from "@/model/enums";
import { downloadText } from "@/utils/file";

// 弹窗显示
export const dlgKeysShow = ref(false);
export const showKeysFormData = ref<any>({
  appId: "",
  keyFormat: KeyFormatEnum.PKCS8,
  publicKeyIsv: "",
  privateKeyIsv: "",
  publicKeyPlatform: "",
  privateKeyPlatform: ""
});
// 表单内容
export const showKeysFormColumns: PlusFormGroupRow[] = [
  {
    title: "基本信息",
    columns: [
      {
        label: "☆AppId",
        prop: "appId",
        labelWidth: 100,
        valueType: "text",
        fieldProps: {
          disabled: true
        }
      },
      {
        label: "秘钥格式",
        prop: "keyFormat",
        labelWidth: 100,
        valueType: "radio",
        options: [
          {
            label: "PKCS8(Java适用)",
            value: KeyFormatEnum.PKCS8
          },
          {
            label: "PKCS1(非Java适用)",
            value: KeyFormatEnum.PKCS1
          }
        ],
        fieldProps: {
          disabled: true
        }
      }
    ]
  },
  {
    title: "ISV公私钥",
    columns: [
      {
        label: "ISV公钥",
        prop: "publicKeyIsv",
        valueType: "textarea",
        labelWidth: 100,
        fieldProps: {
          showWordLimit: false,
          placeholder: "",
          readonly: true,
          autosize: { minRows: 2, maxRows: 4 }
        }
      },
      {
        label: "☆ISV私钥",
        prop: "privateKeyIsv",
        valueType: "textarea",
        labelWidth: 100,
        fieldProps: {
          showWordLimit: false,
          placeholder: "",
          readonly: true,
          autosize: { minRows: 2, maxRows: 4 }
        }
      }
    ]
  },
  {
    title: "平台公私钥",
    columns: [
      {
        label: "☆平台公钥",
        prop: "publicKeyPlatform",
        valueType: "textarea",
        labelWidth: 100,
        fieldProps: {
          showWordLimit: false,
          placeholder: "",
          readonly: true,
          autosize: { minRows: 2, maxRows: 4 }
        }
      },
      {
        label: "平台私钥",
        prop: "privateKeyPlatform",
        valueType: "textarea",
        labelWidth: 100,
        fieldProps: {
          showWordLimit: false,
          placeholder: "",
          readonly: true,
          autosize: { minRows: 2, maxRows: 4 }
        }
      }
    ]
  }
];

export const viewKeys = (row: any) => {
  const params = {
    isvId: row.id
  };
  api.viewKeys(params).then(resp => {
    showKeysFormData.value = resp.data;
    dlgKeysShow.value = true;
  });
};

const downloadTemplate = `AppID:
{appId}

开发者私钥:
{privateKeyIsv}

平台公钥:
{publicKeyPlatform}

`;

export const downloadIsvKey = () => {
  const appId = showKeysFormData.value.appId;
  let text = downloadTemplate
    .replace("{appId}", appId)
    .replace("{privateKeyIsv}", showKeysFormData.value.privateKeyIsv)
    .replace("{publicKeyPlatform}", showKeysFormData.value.publicKeyPlatform);

  downloadText(`${appId}_${new Date().getTime()}.txt`, text);
};
