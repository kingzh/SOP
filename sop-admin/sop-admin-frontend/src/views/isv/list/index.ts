import { onMounted, ref } from "vue";
import {
  type ButtonsCallBackParams,
  type FieldValues,
  type PageInfo,
  type PlusColumn,
  useTable
} from "plus-pro-components";
import { ElMessage } from "element-plus";
import { KeyFormatEnum, StatusEnum } from "@/model/enums";
import { api } from "@/api/isvList";
import { settingKeys } from "@/views/isv/list/isvKeys";
import { settingGroup } from "@/views/isv/list/isvGroup";
export function useIsvList() {
  const isAdd = ref(false);

  // ========= search form =========

  // 查询表单对象
  const searchFormData = ref({
    appId: "",
    status: "",
    pageIndex: 1,
    pageSize: 10
  });

  // 查询表单字段定义
  const searchFormColumns: PlusColumn[] = [
    {
      label: "AppID",
      prop: "appId"
    },
    {
      label: "状态",
      prop: "status",
      valueType: "select",
      options: [
        {
          label: "启用",
          value: StatusEnum.ENABLE,
          color: "green"
        },
        {
          label: "禁用",
          value: StatusEnum.DISABLE,
          color: "red"
        }
      ]
    }
  ];

  // ========= table =========

  // 表格对象
  const {
    tableData,
    total,
    pageInfo,
    buttons: actionButtons
  } = useTable<any[]>();
  // 默认每页条数,默认10
  pageInfo.value.pageSize = 10;

  // 表格字段定义
  const tableColumns: PlusColumn[] = [
    {
      label: "AppID",
      prop: "appId",
      width: 250
    },
    {
      label: "秘钥",
      prop: "keys",
      width: 80
    },
    {
      label: "所属分组",
      prop: "groupNames",
      width: 120,
      tableColumnProps: {
        showOverflowTooltip: true
      }
    },
    {
      label: "状态",
      prop: "status",
      width: 80,
      valueType: "select",
      options: [
        {
          label: "启用",
          value: StatusEnum.ENABLE,
          color: "green"
        },
        {
          label: "禁用",
          value: StatusEnum.DISABLE,
          color: "red"
        }
      ]
    },
    {
      label: "备注",
      prop: "remark",
      tableColumnProps: {
        showOverflowTooltip: true
      }
    },
    {
      width: 100,
      label: "添加人",
      prop: "addBy.showName"
    },
    {
      width: 100,
      label: "修改人",
      prop: "updateBy.showName"
    },
    {
      width: 120,
      label: "添加时间",
      prop: "addTime",
      tableColumnProps: {
        showOverflowTooltip: true
      }
    },
    {
      width: 120,
      label: "修改时间",
      prop: "updateTime",
      tableColumnProps: {
        showOverflowTooltip: true
      }
    }
  ];
  // 表格按钮定义
  actionButtons.value = [
    {
      // 修改
      text: "修改",
      code: "edit",
      props: {
        type: "primary"
      },
      onClick(params: ButtonsCallBackParams) {
        isAdd.value = false;
        editFormData.value = Object.assign({}, params.row);
        dlgTitle.value = "修改";
        dlgShow.value = true;
      }
    },
    {
      text: "设置秘钥",
      code: "edit",
      props: {
        type: "primary"
      },
      onClick(params: ButtonsCallBackParams) {
        settingKeys(params.row);
      }
    },
    {
      text: "设置分组",
      code: "edit",
      props: {
        type: "primary"
      },
      onClick(params: ButtonsCallBackParams) {
        settingGroup(params.row);
      }
    },
    {
      // 启用/禁用
      text: row => (row.status === StatusEnum.ENABLE ? "禁用" : "启用"),
      confirm: {
        message: data => {
          const opt = data.row.status === StatusEnum.ENABLE ? "禁用" : "启用";
          return `确定${opt}吗？`;
        },
        options: { draggable: false }
      },
      onConfirm(params: ButtonsCallBackParams) {
        const data = {
          id: params.row.id,
          status:
            params.row.status === StatusEnum.ENABLE
              ? StatusEnum.DISABLE
              : StatusEnum.ENABLE
        };
        api.updateStatus(data).then(() => {
          ElMessage.success("修改成功");
          dlgShow.value = false;
          search();
        });
      }
    }
  ];

  // ========= dialog form =========

  // 弹窗显示
  const dlgShow = ref(false);
  const dlgTitle = ref("");
  // 表单值
  const editFormDataGen = () => {
    return {
      id: 0,
      status: StatusEnum.ENABLE,
      keyFormat: KeyFormatEnum.PKCS8,
      remark: ""
    };
  };
  const editFormData = ref<FieldValues>(editFormDataGen());
  const editFormRules = {};

  // 表单内容
  const editFormColumns: PlusColumn[] = [
    {
      label: "AppID",
      prop: "appId",
      valueType: "copy",
      fieldProps: {
        disabled: true,
        placeholder: "自动生成"
      }
    },
    {
      label: "状态",
      prop: "status",
      valueType: "radio",
      options: [
        {
          label: "启用",
          value: StatusEnum.ENABLE
        },
        {
          label: "禁用",
          value: StatusEnum.DISABLE
        }
      ]
    },
    {
      label: "备注",
      prop: "remark",
      valueType: "textarea",
      fieldProps: {
        maxlength: 300,
        showWordLimit: true,
        autosize: { minRows: 2, maxRows: 4 }
      }
    }
  ];

  // ========= event =========

  // 添加按钮事件
  const handleAdd = () => {
    isAdd.value = true;
    editFormData.value = editFormDataGen();
    dlgTitle.value = "新增";
    dlgShow.value = true;
  };

  // 保存按钮事件,校验成功后触发
  const handleSave = () => {
    const postData = editFormData.value;
    const pms = isAdd.value ? api.add(postData) : api.update(postData);
    pms.then(() => {
      ElMessage.success("保存成功");
      dlgShow.value = false;
      search();
    });
  };

  // 点击查询按钮
  const handleSearch = () => {
    pageInfo.value.page = 1;
    search();
  };

  // 分页事件
  const handlePaginationChange = (_pageInfo: PageInfo): void => {
    pageInfo.value = _pageInfo;
    search();
  };

  // 查询
  const search = async () => {
    try {
      const { data } = await doSearch();
      tableData.value = data.list;
      total.value = data.total;
    } catch (error) {}
  };
  // 请求接口
  const doSearch = async () => {
    // 查询参数
    const data = searchFormData.value;
    // 添加分页参数
    data.pageIndex = pageInfo.value.page;
    data.pageSize = pageInfo.value.pageSize;

    return api.page(data);
  };

  onMounted(() => {
    // 页面加载
    search();
  });

  return {
    search,
    actionButtons,
    dlgShow,
    dlgTitle,
    editFormColumns,
    editFormData,
    editFormRules,
    handleAdd,
    handlePaginationChange,
    handleSave,
    handleSearch,
    pageInfo,
    searchFormColumns,
    searchFormData,
    tableColumns,
    tableData,
    total
  };
}
