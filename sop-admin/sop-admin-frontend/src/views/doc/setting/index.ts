import { onMounted, ref } from "vue";
import type { PlusColumn, FieldValues } from "plus-pro-components";
import { api } from "@/api/docSetting";
import { ElMessage } from "element-plus";

export function useDocSetting() {
  const state = ref<FieldValues>({
    tornaServerAddr: "",
    openProdUrl: "",
    openSandboxUrl: ""
  });

  const rules = {};

  const columns: PlusColumn[] = [
    {
      label: "Torna地址",
      prop: "tornaServerAddr",
      valueType: "copy",
      fieldProps: {
        placeholder: "Torna地址,到端口号,如:https://torna.xxx.com:7700"
      }
    },
    {
      label: "开放平台线上环境地址",
      prop: "openProdUrl",
      valueType: "copy",
      fieldProps: {
        placeholder: "开放平台线上环境地址,如:https://open.xxx.com/api"
      }
    },
    {
      label: "开放平台沙箱环境地址",
      prop: "openSandboxUrl",
      valueType: "copy",
      fieldProps: {
        placeholder: "开放平台沙箱环境地址, 如:https://open-sandbox.xxx.com/api"
      }
    }
  ];

  const handleSubmit = (values: FieldValues) => {
    api.save(values).then(() => {
      ElMessage.success("保存成功");
    });
  };

  const loadSetting = () => {
    api.get().then(resp => {
      state.value = resp.data;
    });
  };

  onMounted(() => {
    loadSetting();
  });

  return { state, rules, columns, handleSubmit };
}
