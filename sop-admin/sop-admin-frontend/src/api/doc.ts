import { createUrl, http } from "@/utils/http";
import type { Result } from "@/model";

// 后端请求接口
const apiUrl: any = createUrl({
  addApp: "/doc/app/add",
  syncAppDoc: "/doc/app/syncAppDoc",
  syncDoc: "/doc/app/syncDoc",
  listApp: "/doc/app/list",
  listDocTree: "/doc/info/tree",
  publish: "/doc/info/publish"
});

interface DocApp {
  id: number;
  appName: string;
}

/**
 * 接口管理
 */
export const api: any = {
  /**
   * 分页查询
   */
  listApp(): Promise<Result<Array<DocApp>>> {
    return http.get<Result<Array<DocApp>>, any>(apiUrl.listApp, {});
  },
  /**
   * 新增
   * @param data 表单内容
   */
  addApp(data: object) {
    return http.post<Result<any>, any>(apiUrl.addApp, { data });
  },
  /**
   * 同步文档
   * @param data 表单内容
   */
  syncAppDoc(docAppId) {
    const data = {
      id: docAppId
    };
    return http.post<Result<any>, any>(apiUrl.syncAppDoc, { data });
  },
  syncDoc(docInfoId) {
    const data = {
      id: docInfoId
    };
    return http.post<Result<any>, any>(apiUrl.syncDoc, { data });
  },
  /**
   * 发布
   * @param data 表单内容
   */
  publish(data: object) {
    return http.post<Result<any>, any>(apiUrl.publish, { data });
  },
  /**
   * 查询文档树
   * @param data
   */
  listDocTree(params: object) {
    return http.get<Result<Array<any>>, any>(apiUrl.listDocTree, { params });
  }
};
