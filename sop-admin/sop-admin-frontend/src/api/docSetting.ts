import { createUrl, http } from "@/utils/http";
import type { PageResult, Result } from "@/model";

// 后端请求接口
const apiUrl: any = createUrl({
  get: "/doc/setting/get",
  save: "/doc/setting/save"
});

/**
 * 接口管理
 */
export const api: any = {
  /**
   * 分页查询
   */
  get(): Promise<PageResult> {
    return http.get<PageResult, any>(apiUrl.get, {});
  },
  /**
   * 新增
   * @param data 表单内容
   */
  save(data: object) {
    return http.post<Result<any>, any>(apiUrl.save, { data });
  }
};
