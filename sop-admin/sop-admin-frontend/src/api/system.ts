import { baseUrl, http } from "@/utils/http";

type Result = {
  success: boolean;
  data?: Array<any>;
};

type ResultTable = {
  success: boolean;
  data?: {
    /** 列表数据 */
    list: Array<any>;
    /** 总条目数 */
    total?: number;
    /** 每页显示条目个数 */
    pageSize?: number;
    /** 当前页数 */
    pageIndex?: number;
  };
};

/** 获取系统管理-用户管理列表 */
export const getUserList = (params?: object) => {
  return http.request<ResultTable>("get", baseUrl("sys/user/page"), { params });
};

/** 获取系统管理-用户管理-添加 */
export const addUser = (data?: object) => {
  return http.request<ResultTable>("post", baseUrl("sys/user/add"), {
    data
  });
};

/** 获取系统管理-用户管理-修改 */
export const updateUser = (data?: object) => {
  return http.request<ResultTable>("post", baseUrl("sys/user/update"), {
    data
  });
};

/** 获取系统管理-用户管理-修改状态 */
export const updateUserStatus = (data?: object) => {
  return http.request<ResultTable>("post", baseUrl("sys/user/updateStatus"), {
    data
  });
};

/** 获取系统管理-用户管理-重置密码 */
export const resetPassword = (data?: object) => {
  return http.request<ResultTable>("post", baseUrl("sys/user/resetPassword"), {
    data
  });
};

/** 获取系统管理-用户管理-批量修改状态 */
export const updateStatusBatch = (data?: object) => {
  return http.request<ResultTable>(
    "post",
    baseUrl("sys/user/updateStatusBatch"),
    { data }
  );
};

/** 系统管理-用户管理-根据userId，获取对应角色id列表（userId：用户id） */
export const getRoleIds = (params?: object) => {
  return http.request<Result>("get", baseUrl("sys/userrole/getUserRoleIds"), {
    params
  });
};

/** 获取系统管理-用户管理-设置用户角色 */
export const setUserRole = (data?: object) => {
  return http.request<ResultTable>(
    "post",
    baseUrl("sys/userrole/setUserRole"),
    { data }
  );
};

/** 系统管理-用户管理-获取所有角色列表 */
export const getAllRoleList = () => {
  return http.request<Result>("get", baseUrl("sys/role/all"));
};

/** 获取系统管理-角色管理列表 */
export const getRoleList = (params?: object) => {
  return http.request<ResultTable>("get", baseUrl("sys/role/page"), { params });
};

/** 添加角色 */
export const addRole = (data?: object) => {
  return http.request<ResultTable>("post", baseUrl("sys/role/add"), { data });
};

/** 删除角色状态 */
export const updateRoleStatus = (data?: object) => {
  return http.request<ResultTable>("post", baseUrl("sys/role/updateStatus"), {
    data
  });
};

/** 修改角色 */
export const updateRole = (data?: object) => {
  return http.request<ResultTable>("post", baseUrl("sys/role/update"), {
    data
  });
};

/** 删除角色 */
export const delRole = (data?: object) => {
  return http.request<ResultTable>("post", baseUrl("sys/role/delete"), {
    data
  });
};

/** 获取系统管理-菜单管理列表 */
export const getMenuList = (data?: object) => {
  return http.request<Result>("get", baseUrl("sys/resource/listAll"), {
    data
  });
};

/** 获取系统管理-菜单管理-添加 */
export const addMenu = (data?: object) => {
  return http.request<Result>("post", baseUrl("sys/resource/add"), { data });
};

/** 获取系统管理-菜单管理-修改 */
export const updateMenu = (data?: object) => {
  return http.request<Result>("post", baseUrl("sys/resource/update"), { data });
};

/** 获取系统管理-菜单管理-删除 */
export const deleteMenu = (data?: object) => {
  return http.request<Result>("post", baseUrl("sys/resource/delete"), { data });
};

/** 系统管理-角色管理-保存角色菜单 */
export const saveRoleMenu = (data?: object) => {
  return http.request<Result>(
    "post",
    baseUrl("sys/resource/saveRoleResource"),
    { data }
  );
};

/** 获取系统管理-部门管理列表 */
export const getDeptList = (params?: object) => {
  return http.request<Result>("get", baseUrl("sys/dept/listAll"), { params });
};

/** 获取系统管理-添加部门 */
export const addDept = (data?: object) => {
  return http.request<Result>("post", baseUrl("sys/dept/add"), { data });
};

/** 获取系统管理-修改部门 */
export const updateDept = (data?: object) => {
  return http.request<Result>("post", baseUrl("sys/dept/update"), { data });
};

/** 获取系统监控-在线用户列表 */
export const getOnlineLogsList = (data?: object) => {
  return http.request<ResultTable>("post", "/online-logs", { data });
};

/** 获取系统监控-登录日志列表 */
export const getLoginLogsList = (data?: object) => {
  return http.request<ResultTable>("post", "/login-logs", { data });
};

/** 获取系统监控-操作日志列表 */
export const getOperationLogsList = (data?: object) => {
  return http.request<ResultTable>("post", "/operation-logs", { data });
};

/** 获取系统监控-系统日志列表 */
export const getSystemLogsList = (data?: object) => {
  return http.request<ResultTable>("post", "/system-logs", { data });
};

/** 获取系统监控-系统日志-根据 id 查日志详情 */
export const getSystemLogsDetail = (data?: object) => {
  return http.request<Result>("post", "/system-logs-detail", { data });
};

/** 获取角色管理-权限-菜单权限 */
export const getRoleMenu = () => {
  return http.request<Result>("get", baseUrl("sys/resource/listAll"), {});
};

/** 获取角色管理-权限-菜单权限-根据角色 id 查对应菜单 */
export const getRoleMenuIds = (params?: object) => {
  return http.request<Result>("get", baseUrl("sys/resource/listRoleResource"), {
    params
  });
};
