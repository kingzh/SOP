import { createUrl, http } from "@/utils/http";
import type { PageResult, Result } from "@/model";

// 后端请求接口
const apiUrl: any = createUrl({
  page: "perm/group/permission/page",
  setting: "perm/group/permission/setting",
  del: "perm/group/permission/delete"
});

/**
 * 接口管理
 */
export const api: any = {
  /**
   * 查询分组权限
   *
   * @param params params
   */
  page(params): Promise<PageResult> {
    return http.get<PageResult, any>(apiUrl.page, { params });
  },
  /**
   * 设置接口权限
   *
   * @param groupId groupId
   * @param apiIdList apiIdList
   */
  setting(groupId, apiIdList) {
    const data = {
      groupId: groupId,
      apiIdList: apiIdList
    };
    return http.post<Result<any>, any>(apiUrl.setting, { data });
  },
  /**
   * 删除
   * @param data 表单内容
   */
  del(data: object) {
    return http.post<Result<any>, any>(apiUrl.del, { data });
  }
};
