package com.gitee.sop.admin.common.exception;

/**
 * @author tanghc
 */
public class JwtExpiredException extends Exception {
    @Override
    public String getMessage() {
        return "jwt expired";
    }
}
