package com.gitee.sop.admin.common.jackson.convert.serde;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Objects;

/**
 * <pre>
 * {@literal
 * 将数字类型转换成布尔
 *
 * @Bool
 * private Integer isLoading; // 返回给前端: "isLoading": true/false
 *
 * }
 * </pre>
 *
 * @author 六如
 */
@Slf4j
public class BoolSerializer extends JsonSerializer<Object> {

    private static final String TRUE_VALUE = "1";

    @Override
    public void serialize(Object value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (value == null) {
            jsonGenerator.writeNull();
            return;
        }
        jsonGenerator.writeBoolean(Objects.equals(TRUE_VALUE, String.valueOf(value)));
    }

}
