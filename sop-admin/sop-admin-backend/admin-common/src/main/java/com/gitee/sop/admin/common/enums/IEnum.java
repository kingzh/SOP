package com.gitee.sop.admin.common.enums;

import java.io.Serializable;

/**
 * @author 六如
 * @param <T> 参数类型
 */
public interface IEnum<T extends Serializable> {

    /**
     * 获取值
     *
     * @return 返回枚举值
     */
    T getValue();

    /**
     * 获取描述
     *
     * @return 返回枚举描述
     */
    String getDescription();

}
