package com.gitee.sop.admin.common.exception;

/**
 * @author tanghc
 */
public interface ExceptionCode {
    ErrorCode getCode();
}
