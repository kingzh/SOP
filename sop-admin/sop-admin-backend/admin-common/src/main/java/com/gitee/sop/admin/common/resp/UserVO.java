package com.gitee.sop.admin.common.resp;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class UserVO {

    private Long id;

    private String username;

    private String nickname;

    private String showName;

}
