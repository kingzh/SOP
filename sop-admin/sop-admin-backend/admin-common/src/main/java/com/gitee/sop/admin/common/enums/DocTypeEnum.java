package com.gitee.sop.admin.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 文档类型,0:http,1:dubbo,2:富文本,3:Markdown
 *
 * @author 六如
 */
@Getter
@AllArgsConstructor
public enum DocTypeEnum implements IntEnum {
    HTTP(0, "HTTP"),
    DUBBO(1, "dubbo"),
    RICH_TEXT(2, "富文本"),
    MARKDOWN(3, "Markdown");
    private final Integer value;
    private final String description;

}
