package com.gitee.sop.admin.common.user;

/**
 * 登录用户信息
 * @author 六如
 */
public interface User {

    /**
     * 用户id
     * @return
     */
    Long getUserId();

    /**
     * 返回username
     * @return
     */
    String getUsername();

    /**
     * 昵称
     * @return
     */
    String getNickname();

    Integer getStatus();

    String getToken();

    default String getShowName() {
        String nickname = getNickname();
        return nickname != null && !nickname.isEmpty() ? nickname : getUsername();
    }
}
