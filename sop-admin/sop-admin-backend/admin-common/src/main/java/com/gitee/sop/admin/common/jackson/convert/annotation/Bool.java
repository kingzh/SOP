package com.gitee.sop.admin.common.jackson.convert.annotation;


import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gitee.sop.admin.common.jackson.convert.serde.BoolDeserializer;
import com.gitee.sop.admin.common.jackson.convert.serde.BoolSerializer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 序列化自动转换成boolean值,反序列化转换成数字
 * @author 六如
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@JacksonAnnotationsInside
@JsonSerialize(using = BoolSerializer.class)
@JsonDeserialize(using = BoolDeserializer.class)
public @interface Bool {
}
