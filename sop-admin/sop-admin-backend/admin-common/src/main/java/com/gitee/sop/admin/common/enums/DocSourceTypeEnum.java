package com.gitee.sop.admin.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 来源类型,1-torna,2-自建
 *
 * @author 六如
 */
@Getter
@AllArgsConstructor
public enum DocSourceTypeEnum implements IntEnum {
    TORNA(1, "Torna"),
    CUSTOM(2, "自建");
    private final Integer value;
    private final String description;

}
