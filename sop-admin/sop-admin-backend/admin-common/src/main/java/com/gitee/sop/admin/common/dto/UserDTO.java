package com.gitee.sop.admin.common.dto;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class UserDTO {

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 添加人姓名
     */
    private String addByName;

    /**
     * 修改人id
     */
    private Long updateBy;

    /**
     * 修改人姓名
     */
    private String updateByName;

}
