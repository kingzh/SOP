package com.gitee.sop.admin.common.req;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @author 六如
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class StatusUpdateBatchParam extends IdsParam {

    @NotNull(message = "状态不能为空")
    private Integer status;

}
