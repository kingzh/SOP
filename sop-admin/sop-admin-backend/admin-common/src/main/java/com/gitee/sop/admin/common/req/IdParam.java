package com.gitee.sop.admin.common.req;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 六如
 */
@Data
public class IdParam {
    @NotNull(message = "id不能为空")
    private Long id;
}
