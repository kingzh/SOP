package com.gitee.sop.admin.common.jackson.convert.serde;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Objects;

/**
 * <pre>
 * {@literal
 * 将布尔值类型转换成数字
 *
 * @IntBool
 * private Integer isLoading;
 * }
 * </pre>
 *
 * @author 六如
 */
@Slf4j
public class BoolDeserializer extends StdDeserializer<Object> {
    private static final long serialVersionUID = -4993230470571124275L;

    private static final String TRUE = "true";


    public BoolDeserializer() {
        this(null);
    }

    protected BoolDeserializer(final Class<?> vc) {
        super(vc);
    }


    @Override
    public Object deserialize(final JsonParser p, final DeserializationContext ctxt) throws IOException {
        String text = p.getText();
        return Objects.equals(TRUE, text) ? 1 : 0;
    }
}
