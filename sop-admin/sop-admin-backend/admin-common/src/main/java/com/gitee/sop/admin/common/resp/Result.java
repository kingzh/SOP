package com.gitee.sop.admin.common.resp;

import lombok.Data;

import java.util.Objects;

/**
 * @param <T> 数据
 * @author thc
 */
@Data
public class Result<T> {

    private static final Result<?> RESULT = new Result<>();

    private String code = "0";
    private T data;
    private String msg = "success";

    public static Result<?> ok() {
        return RESULT;
    }

    public static <E> Result<E> ok(E obj) {
        Result<E> result = new Result<>();
        result.setData(obj);
        return result;
    }

    public static <E> Result<E> err(String msg) {
        Result<E> result = new Result<>();
        result.setCode("1");
        result.setMsg(msg);
        return result;
    }

    public static <E> Result<E> err(String code, String msg) {
        Result<E> result = new Result<>();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public boolean getSuccess() {
        return Objects.equals("0", code);
    }
}
