package com.gitee.sop.admin.common;

import com.gitee.sop.admin.common.util.RSATool;
import org.junit.Test;

/**
 * @author 六如
 */
public class RSAToolTest {

    @Test
    public void create() throws Exception {
        RSATool.KeyStore keys = new RSATool(RSATool.KeyFormat.PKCS1, RSATool.KeyLength.LENGTH_2048).createKeys();
        System.out.println(keys.getPrivateKey());
        System.out.println(keys.getPublicKey());
    }


}
