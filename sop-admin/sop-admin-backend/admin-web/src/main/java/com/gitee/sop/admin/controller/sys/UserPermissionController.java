package com.gitee.sop.admin.controller.sys;

import com.gitee.sop.admin.common.context.UserContext;
import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.service.sys.UserPermissionService;
import com.gitee.sop.admin.service.sys.dto.MenuTreeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 六如
 */
@RestController
@RequestMapping("sys/userperm")
public class UserPermissionController {

    @Autowired
    private UserPermissionService userPermissionService;

    /**
     * 获取当前登录用户菜单
     *
     * @return 返回菜单树
     */
    @GetMapping("listCurrentUserMenu")
    public Result<List<MenuTreeDTO>> listCurrentUserMenu() {
        List<MenuTreeDTO> menuTreeDTOS = userPermissionService.listUserMenuTree(UserContext.getUserId());
        return Result.ok(menuTreeDTOS);
    }

}
