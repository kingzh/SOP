package com.gitee.sop.admin.controller.isv;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.sop.admin.common.req.IdParam;
import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.controller.isv.param.PermGroupPageParam;
import com.gitee.sop.admin.controller.isv.param.PermGroupParam;
import com.gitee.sop.admin.dao.entity.PermGroup;
import com.gitee.sop.admin.service.isv.PermGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 权限分组
 *
 * @author 六如
 */
@RestController
@RequestMapping("perm/group")
public class PermGroupController {

    @Autowired
    private PermGroupService permGroupService;

    /**
     * 查询全部
     *
     * @return 返回分页结果
     */
    @GetMapping("/listAll")
    public Result<List<PermGroup>> listAll(PermGroupParam param) {
        List<PermGroup> list = permGroupService.list(param.toQuery());
        return Result.ok(list);
    }

    /**
     * 分页查询
     *
     * @param param 查询参数
     * @return 返回分页结果
     */
    @GetMapping("/page")
    public Result<PageInfo<PermGroup>> page(PermGroupPageParam param) {
        LambdaQuery<PermGroup> query = param.toLambdaQuery(PermGroup.class);
        PageInfo<PermGroup> pageInfo = permGroupService.doPage(query);
        return Result.ok(pageInfo);
    }

    /**
     * 新增记录
     *
     * @param permGroup 表单参数
     * @return 返回添加后的主键值
     */
    @PostMapping("/add")
    public Result<Long> add(@Validated @RequestBody PermGroup permGroup) {
        permGroupService.save(permGroup);
        // 返回添加后的主键值
        return Result.ok(permGroup.getId());
    }

    /**
     * 修改记录
     *
     * @param permGroup 表单数据
     * @return 返回影响行数
     */
    @PostMapping("/update")
    public Result<Integer> update(@Validated @RequestBody PermGroup permGroup) {
        return Result.ok(permGroupService.update(permGroup));
    }

    /**
     * 删除记录
     *
     * @param param 参数
     * @return 返回影响行数
     */
    @PostMapping("/delete")
    public Result<Integer> delete(@Validated @RequestBody IdParam param) {
        return Result.ok(permGroupService.delete(param.getId()));
    }


}
