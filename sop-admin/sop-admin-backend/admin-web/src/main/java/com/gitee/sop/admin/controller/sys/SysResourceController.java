package com.gitee.sop.admin.controller.sys;

import com.gitee.sop.admin.common.context.UserContext;
import com.gitee.sop.admin.common.req.IdParam;
import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.controller.sys.param.SysResourceAddParam;
import com.gitee.sop.admin.controller.sys.param.SysResourceUpdateParam;
import com.gitee.sop.admin.controller.sys.param.SysRoleResourceSaveParam;
import com.gitee.sop.admin.controller.sys.vo.SysResourceVO;
import com.gitee.sop.admin.dao.entity.SysResource;
import com.gitee.sop.admin.service.sys.SysResourceService;
import com.gitee.sop.admin.service.sys.SysRoleResourceService;
import com.gitee.sop.admin.service.sys.dto.SysRoleResourceDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 资源菜单
 *
 * @author 六如
 */
@RestController
@RequestMapping("sys/resource")
public class SysResourceController {

    @Autowired
    private SysResourceService sysResourceService;
    @Autowired
    private SysRoleResourceService sysRoleResourceService;

    /**
     * 获取菜单树
     *
     * @return 返回分页结果
     */
    @GetMapping("/tree")
    public Result<List<SysResourceVO>> tree() {
        List<SysResource> list = sysResourceService.listAll();
        List<SysResourceVO> retList = CopyUtil.copyList(list, SysResourceVO::new);
        return Result.ok(retList);
    }

    /**
     * 查询全部资源
     *
     * @return 返回分页结果
     */
    @GetMapping("/listAll")
    public Result<List<SysResourceVO>> listAll() {
        List<SysResource> list = sysResourceService.listAll();
        List<SysResourceVO> retList = CopyUtil.copyList(list, SysResourceVO::new);
        return Result.ok(retList);
    }


    /**
     * 新增记录
     *
     * @param param 表单参数
     * @return 返回添加后的主键值
     */
    @PostMapping("/add")
    public Result<Long> add(@Validated @RequestBody SysResourceAddParam param) {
        SysResource sysResource = CopyUtil.copyBean(param, SysResource::new);
        sysResourceService.save(sysResource);
        // 返回添加后的主键值
        return Result.ok(sysResource.getId());
    }

    /**
     * 修改记录
     *
     * @param param 表单数据
     * @return 返回影响行数
     */
    @PostMapping("/update")
    public Result<Integer> update(@Validated @RequestBody SysResourceUpdateParam param) {
        SysResource sysResource = CopyUtil.copyBean(param, SysResource::new);
        return Result.ok(sysResourceService.update(sysResource));
    }

    /**
     * 删除记录
     *
     * @param param 参数
     * @return 返回影响行数
     */
    @PostMapping("/delete")
    public Result<Integer> delete(@Validated @RequestBody IdParam param) {
        return Result.ok(sysResourceService.deleteById(param.getId()));
    }

    /**
     * 保存角色资源
     *
     * @param param 参数
     * @return 返回影响行数
     */
    @PostMapping("/saveRoleResource")
    public Result<Integer> saveRoleResource(@Validated @RequestBody SysRoleResourceSaveParam param) {
        SysRoleResourceDTO sysRoleResourceDTO = CopyUtil.copyBean(param, SysRoleResourceDTO::new);
        sysRoleResourceDTO.setAddBy(UserContext.getUser().getUserId());
        int cnt = sysRoleResourceService.saveRoleResource(sysRoleResourceDTO);
        return Result.ok(cnt);
    }

    /**
     * 获取角色资源
     *
     * @param param 参数
     * @return 返回影响行数
     */
    @GetMapping("/listRoleResource")
    public Result<List<Long>> listRoleResource(@Validated IdParam param) {
        List<Long> resourceIds = sysRoleResourceService.listRoleResource(param.getId());
        return Result.ok(resourceIds);
    }


}
