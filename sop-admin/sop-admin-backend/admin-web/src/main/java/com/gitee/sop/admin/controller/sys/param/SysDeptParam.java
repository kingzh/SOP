package com.gitee.sop.admin.controller.sys.param;

import com.gitee.fastmybatis.core.query.Operator;
import com.gitee.fastmybatis.core.query.annotation.Condition;
import com.gitee.fastmybatis.core.query.param.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 备注：部门表
 *
 * @author 六如
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysDeptParam extends PageParam {
    private static final long serialVersionUID = 7104037961585738100L;

    /**
     * 部门名称
     */
    @Condition(operator = Operator.like)
    private String name;

    /**
     * 状态，1：启用，2：禁用
     */
    @Condition
    private Integer status;

}
