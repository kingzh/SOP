package com.gitee.sop.admin.controller.serve.param;

import com.gitee.fastmybatis.core.query.Operator;
import com.gitee.fastmybatis.core.query.annotation.Condition;
import com.gitee.fastmybatis.core.query.param.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author 六如
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ApiInfoPageParam extends PageParam {

    @Condition(operator = Operator.like)
    private String apiName;

    @Condition
    private Integer status;

    @Condition
    private Integer isPermission;

}
