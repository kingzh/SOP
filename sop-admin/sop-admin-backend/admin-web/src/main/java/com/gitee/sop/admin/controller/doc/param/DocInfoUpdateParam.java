package com.gitee.sop.admin.controller.doc.param;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 六如
 */
@Data
public class DocInfoUpdateParam {

    @NotNull
    private Long id;

    private Integer isPublish;

}
