package com.gitee.sop.admin.controller.sys;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.sop.admin.common.context.UserContext;
import com.gitee.sop.admin.common.dto.StatusUpdateBatchDTO;
import com.gitee.sop.admin.common.dto.StatusUpdateDTO;
import com.gitee.sop.admin.common.req.StatusUpdateBatchParam;
import com.gitee.sop.admin.common.req.StatusUpdateParam;
import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.controller.sys.param.RestPasswordParam;
import com.gitee.sop.admin.controller.sys.param.SysUserAddParam;
import com.gitee.sop.admin.controller.sys.param.SysUserParam;
import com.gitee.sop.admin.controller.sys.param.SysUserUpdateParam;
import com.gitee.sop.admin.controller.sys.vo.SysUserVO;
import com.gitee.sop.admin.service.sys.SysUserService;
import com.gitee.sop.admin.service.sys.dto.SysUserAddDTO;
import com.gitee.sop.admin.service.sys.dto.SysUserDTO;
import com.gitee.sop.admin.service.sys.dto.SysUserSearchDTO;
import com.gitee.sop.admin.service.sys.dto.SysUserUpdateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author 六如
 */
@RestController
@RequestMapping("sys/user")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 分页查询
     *
     * @param param 查询参数
     * @return 返回分页结果
     */
    @GetMapping("/page")
    public Result<PageInfo<SysUserVO>> page(SysUserParam param) {
        SysUserSearchDTO sysUserSearchDTO = CopyUtil.copyBean(param, SysUserSearchDTO::new);
        PageInfo<SysUserDTO> pageInfo = sysUserService.doPage(sysUserSearchDTO);
        PageInfo<SysUserVO> ret = CopyUtil.copyPageInfo(pageInfo, records -> CopyUtil.deepCopyList(records, SysUserVO.class));
        return Result.ok(ret);
    }

    /**
     * 新增记录
     *
     * @param param 表单参数
     * @return 返回添加后的主键值
     */
    @PostMapping("/add")
    public Result<Long> add(@Validated @RequestBody SysUserAddParam param) {
        SysUserAddDTO sysUser = CopyUtil.copyBean(param, SysUserAddDTO::new);
        sysUser.setUpdateBy(UserContext.getUser().getUserId());
        Long id = sysUserService.addUser(sysUser);
        // 返回添加后的主键值
        return Result.ok(id);
    }

    /**
     * 修改记录
     *
     * @param param 表单数据
     * @return 返回影响行数
     */
    @PostMapping("/update")
    public Result<Integer> update(@Validated @RequestBody SysUserUpdateParam param) {
        SysUserUpdateDTO sysUser = CopyUtil.copyBean(param, SysUserUpdateDTO::new);
        sysUser.setUpdateBy(UserContext.getUser().getUserId());
        int cnt = sysUserService.updateUser(sysUser);
        return Result.ok(cnt);
    }

    /**
     * 修改状态
     *
     * @param param 表单数据
     * @return 返回影响行数
     */
    @PostMapping("/updateStatus")
    public Result<Integer> updateStatus(@Validated @RequestBody StatusUpdateParam param) {
        StatusUpdateDTO statusUpdateDTO = CopyUtil.copyBean(param, StatusUpdateDTO::new);
        return Result.ok(sysUserService.updateStatus(statusUpdateDTO));
    }

    /**
     * 修改状态批量
     *
     * @param param 表单数据
     * @return 返回影响行数
     */
    @PostMapping("/updateStatusBatch")
    public Result<Integer> updateStatus(@Validated @RequestBody StatusUpdateBatchParam param) {
        StatusUpdateBatchDTO statusUpdateBatchDTO = CopyUtil.copyBean(param, StatusUpdateBatchDTO::new);
        return Result.ok(sysUserService.updateStatus(statusUpdateBatchDTO));
    }

    /**
     * 重置用户密码
     *
     * @param param 表单数据
     * @return 返回影响行数
     */
    @PostMapping("/resetPassword")
    public Result<Integer> resetPassword(@Validated @RequestBody RestPasswordParam param) {
        sysUserService.resetUserPassword(param.getUserId(), param.getPasswordHash());
        return Result.ok(1);
    }


}
