package com.gitee.sop.admin.controller.sys.param;

import com.gitee.sop.admin.service.sys.dto.SystemConfigDTO;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author 六如
 */
@Data
public class ConfigSettingParam {

    @NotEmpty
    private List<SystemConfigDTO> items;


}
