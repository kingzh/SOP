package com.gitee.sop.admin.controller.sys.param;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;


/**
 *
 * @author 六如
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysUserUpdateParam extends SysUserAddParam {

    /**
     * id
     */
    @NotNull(message = "id必填")
    private Long id;

}
