package com.gitee.sop.admin.controller.doc;

import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.controller.doc.vo.DocSettingVO;
import com.gitee.sop.admin.service.doc.DocSettingService;
import com.gitee.sop.admin.service.doc.dto.DocSettingDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文档设置
 *
 * @author 六如
 */
@RestController
@RequestMapping("/doc/setting")
public class DocSettingController {

    @Autowired
    private DocSettingService docSettingService;

    @GetMapping("get")
    public Result<DocSettingVO> get() {
        DocSettingDTO docSetting = docSettingService.getDocSetting();
        DocSettingVO docSettingVO = CopyUtil.copyBean(docSetting, DocSettingVO::new);
        return Result.ok(docSettingVO);
    }

    @PostMapping("save")
    public Result<Integer> save(@Validated @RequestBody DocSettingDTO docSettingVO) {
        docSettingService.save(docSettingVO);
        return Result.ok(1);
    }

}
