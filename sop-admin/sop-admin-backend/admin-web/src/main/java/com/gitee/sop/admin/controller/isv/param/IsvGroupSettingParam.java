package com.gitee.sop.admin.controller.isv.param;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 六如
 */
@Data
public class IsvGroupSettingParam {

    @NotNull
    private Long isvId;

    private List<Long> groupIds;

}
