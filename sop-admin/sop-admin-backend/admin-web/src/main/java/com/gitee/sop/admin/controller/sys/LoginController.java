package com.gitee.sop.admin.controller.sys;

import com.gitee.sop.admin.common.annotation.NoToken;
import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.controller.sys.param.LoginParam;
import com.gitee.sop.admin.controller.sys.vo.LoginResultVO;
import com.gitee.sop.admin.service.sys.login.LoginService;
import com.gitee.sop.admin.service.sys.login.dto.LoginDTO;
import com.gitee.sop.admin.service.sys.login.dto.LoginUser;
import com.gitee.sop.admin.service.sys.login.enums.RegTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录相关接口
 *
 * @author 六如
 */
@RestController
@RequestMapping("sys")
public class LoginController {

    @Autowired
    private LoginService loginService;

    /**
     * 用户登录
     *
     * @param param
     * @return
     */
    @PostMapping("login")
    @NoToken
    public Result<LoginResultVO> login(@Validated @RequestBody LoginParam param) {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername(param.getUsername());
        loginDTO.setPassword(param.getPassword());
        loginDTO.setRegType(RegTypeEnum.BACKEND);
        LoginUser loginUser = loginService.login(loginDTO);
        LoginResultVO loginResult = CopyUtil.copyBean(loginUser, LoginResultVO::new);
        return Result.ok(loginResult);
    }
}
