package com.gitee.sop.admin.controller.doc.param;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author 六如
 */
@Data
public class DocAppAddParam {

    @NotBlank
    @Length(max = 128)
    private String tornaToken;

}
