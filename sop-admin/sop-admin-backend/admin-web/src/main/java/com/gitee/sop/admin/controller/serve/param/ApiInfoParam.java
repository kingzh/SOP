package com.gitee.sop.admin.controller.serve.param;

import com.gitee.fastmybatis.core.query.Operator;
import com.gitee.fastmybatis.core.query.annotation.Condition;
import lombok.Data;

/**
 * @author 六如
 */
@Data
public class ApiInfoParam {

    @Condition(operator = Operator.like)
    private String apiName;

    @Condition
    private Integer status;

}
