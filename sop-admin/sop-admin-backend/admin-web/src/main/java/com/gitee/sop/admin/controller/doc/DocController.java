package com.gitee.sop.admin.controller.doc;

import com.gitee.sop.admin.common.context.UserContext;
import com.gitee.sop.admin.common.req.IdParam;
import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.controller.doc.param.DocAppAddParam;
import com.gitee.sop.admin.controller.doc.param.DocInfoUpdateParam;
import com.gitee.sop.admin.controller.doc.vo.DocAppVO;
import com.gitee.sop.admin.controller.doc.vo.DocInfoTreeVO;
import com.gitee.sop.admin.service.doc.DocAppService;
import com.gitee.sop.admin.service.doc.DocInfoService;
import com.gitee.sop.admin.service.doc.DocInfoSyncService;
import com.gitee.sop.admin.service.doc.dto.DocAppDTO;
import com.gitee.sop.admin.service.doc.dto.DocInfoPublishUpdateDTO;
import com.gitee.sop.admin.service.doc.dto.DocInfoTreeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 六如
 */
@RestController
@RequestMapping("doc")
public class DocController {

    @Autowired
    private DocInfoService docInfoService;
    @Autowired
    private DocAppService docAppService;
    @Autowired
    private DocInfoSyncService docInfoSyncService;

    @GetMapping("app/list")
    public Result<List<DocAppVO>> listApp() {
        List<DocAppDTO> docAppDTOS = docAppService.listDocApp();
        List<DocAppVO> docAppVOS = CopyUtil.copyList(docAppDTOS, DocAppVO::new);
        return Result.ok(docAppVOS);
    }

    @PostMapping("app/add")
    public Result<Long> addApp(@Validated @RequestBody DocAppAddParam param) {
        Long docAppId = docAppService.addDocApp(param.getTornaToken(), UserContext.getUser());
        return Result.ok(docAppId);
    }

    @PostMapping("app/syncAppDoc")
    public Result<Integer> syncAppDoc(@Validated @RequestBody IdParam param) {
        docInfoSyncService.syncAppDoc(param.getId(), UserContext.getUser());
        return Result.ok(1);
    }

    @PostMapping("app/syncDoc")
    public Result<Integer> syncDoc(@Validated @RequestBody IdParam param) {
        docInfoSyncService.syncDoc(param.getId(), UserContext.getUser());
        return Result.ok(1);
    }

    @GetMapping("info/tree")
    public Result<List<DocInfoTreeVO>> docTree(@RequestParam Long docAppId) {
        List<DocInfoTreeDTO> docInfoTreeDTOS = docInfoService.listDocTree(docAppId);
        List<DocInfoTreeVO> docInfoTreeVOS = CopyUtil.deepCopyList(docInfoTreeDTOS, DocInfoTreeVO.class);
        return Result.ok(docInfoTreeVOS);
    }

    @PostMapping("info/publish")
    public Result<Integer> publish(@Validated @RequestBody DocInfoUpdateParam param) {
        int cnt = docInfoService.publish(CopyUtil.copyBean(param, DocInfoPublishUpdateDTO::new));
        return Result.ok(cnt);
    }

}
