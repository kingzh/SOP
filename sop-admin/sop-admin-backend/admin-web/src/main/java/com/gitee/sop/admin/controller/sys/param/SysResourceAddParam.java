package com.gitee.sop.admin.controller.sys.param;

import com.gitee.sop.admin.common.jackson.convert.annotation.Bool;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


/**
 *
 * @author 六如
 */
@Data
public class SysResourceAddParam {

    /**
     * 菜单类型（0代表菜单、1代表iframe、2代表外链、3代表按钮）
     */
    @NotNull(message = "菜单类型必填")
    private Integer menuType;

    /**
     * 菜单名称, menuType=0时必填
     */
    private String title;

    /**
     * 路由名称, menuType=0时必填
     */
    private String name;

    /**
     * 路由路径, menuType=0时必填
     */
    private String path;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 排序
     */
    private Integer rank;

    /**
     * 路由重定向
     */
    private String redirect;

    /**
     * 路由重定向
     */
    private String icon;

    /**
     * 右侧图标
     */
    private String extraIcon;

    /**
     * 进场动画（页面加载动画）
     */
    private String enterTransition;

    /**
     * 离场动画（页面加载动画）
     */
    private String leaveTransition;

    /**
     * 菜单激活
     */
    private String activePath;

    /**
     * 权限标识
     */
    private String auths;

    /**
     * 链接地址（需要内嵌的`iframe`链接地址）
     */
    private String frameSrc;

    /**
     * 加载动画（内嵌的`iframe`页面是否开启首次加载动画）
     */
    @Bool
    private Integer frameLoading;

    /**
     * 缓存页面
     */
    @Bool
    private Integer keepAlive;

    /**
     * 标签页（当前菜单名称或自定义信息禁止添加到标签页）
     */
    @Bool
    private Integer hiddenTag;

    /**
     * 固定标签页（当前菜单名称是否固定显示在标签页且不可关闭）
     */
    @Bool
    private Integer fixedTag;

    /**
     * 菜单（是否显示该菜单）
     */
    @Bool
    private Integer showLink;

    /**
     * 父级菜单（是否显示父级菜单
     */
    @Bool
    private Integer showParent;

    /**
     * 父级id
     */
    private Long parentId;

    /**
     * 是否删除
     */
    private Integer isDeleted;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 修改人id
     */
    private Long updateBy;


}
