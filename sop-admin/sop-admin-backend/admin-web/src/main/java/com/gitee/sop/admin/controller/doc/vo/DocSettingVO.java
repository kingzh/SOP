package com.gitee.sop.admin.controller.doc.vo;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class DocSettingVO {

    private String tornaServerAddr;

    private String openProdUrl;

    private String openSandboxUrl;



}
