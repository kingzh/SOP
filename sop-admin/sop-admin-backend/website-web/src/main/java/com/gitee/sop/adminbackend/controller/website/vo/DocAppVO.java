package com.gitee.sop.admin.controller.website.vo;

import lombok.Data;


/**
 * 备注：文档应用
 *
 * @author 六如
 */
@Data
public class DocAppVO {

    /**
     * id
     */
    private Long id;

    /**
     * 应用名称
     */
    private String appName;

}
