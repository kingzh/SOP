package com.gitee.sop.admin.controller.website.vo;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class DocInfoConfigVO {

    private String openProdUrl;
    private String openSandboxUrl;

}
