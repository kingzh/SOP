package com.gitee.sop.admin.controller.website.vo;

import com.gitee.fastmybatis.core.support.TreeNode;
import com.gitee.sop.admin.common.constants.YesOrNo;
import lombok.Data;

import java.util.List;
import java.util.Objects;


/**
 * 备注：文档信息
 *
 * @author 六如
 */
@Data
public class DocInfoTreeVO implements TreeNode<DocInfoTreeVO, Long> {

    /**
     * id
     */
    private Long id;

    /**
     * doc_app.id
     */
    private Long docAppId;

    /**
     * 文档id
     */
    private Long docId;

    /**
     * 文档标题
     */
    private String docTitle;

    /**
     * 文档code
     */
    private String docCode;

    /**
     * 文档类型,1-dubbo,2-富文本,3-Markdown
     */
    private Integer docType;

    /**
     * 来源类型,1-torna,2-自建
     */
    private Integer sourceType;

    /**
     * 文档名称
     */
    private String docName;

    /**
     * 版本号
     */
    private String docVersion;

    /**
     * 描述
     */
    private String description;

    /**
     * 是否分类
     */
    private Integer isFolder;

    /**
     * 父节点id
     */
    private Long parentId;


    private List<DocInfoTreeVO> children;


    @Override
    public Long takeId() {
        return docId;
    }

    @Override
    public Long takeParentId() {
        return parentId;
    }


    public String getDocName() {
        if (Objects.equals(isFolder, YesOrNo.YES)) {
            return "";
        }
        return docName;
    }
}
