package com.gitee.sop.admin.controller.website;

import com.gitee.sop.admin.common.annotation.NoToken;
import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.controller.website.vo.DocAppVO;
import com.gitee.sop.admin.controller.website.vo.DocInfoViewVO;
import com.gitee.sop.admin.controller.website.vo.DocInfoTreeVO;
import com.gitee.sop.admin.service.doc.dto.DocAppDTO;
import com.gitee.sop.admin.service.doc.dto.DocInfoTreeDTO;
import com.gitee.sop.admin.service.doc.dto.DocInfoViewDTO;
import com.gitee.sop.admin.service.website.WebsiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 提供给网站的接口,不需要校验token
 *
 * @author 六如
 */
@RestController
@RequestMapping("website")
@NoToken
public class WebsiteController {

    @Autowired
    private WebsiteService websiteService;

    /**
     * 获取文档应用列表
     */
    @GetMapping("docapp/list")
    public Result<List<DocAppVO>> listDocApp() {
        List<DocAppDTO> docAppDTOS = websiteService.listDocApp();
        List<DocAppVO> docAppVOS = CopyUtil.deepCopyList(docAppDTOS, DocAppVO.class);
        return Result.ok(docAppVOS);
    }

    /**
     * 获取文档菜单树
     *
     * @param docAppId 应用id
     */
    @GetMapping("docinfo/tree")
    public Result<List<DocInfoTreeVO>> listDocMenuTree(Long docAppId) {
        List<DocInfoTreeDTO> docInfoTreeDTOS = websiteService.listDocMenuTree(docAppId);
        List<DocInfoTreeVO> docAppVOS = CopyUtil.deepCopyList(docInfoTreeDTOS, DocInfoTreeVO.class);
        return Result.ok(docAppVOS);
    }

    /**
     * 获取文档详情
     *
     * @param id id
     */
    @GetMapping("docinfo/detail")
    public Result<DocInfoViewVO> getDocDetail(Long id) {
        DocInfoViewDTO docInfoViewDTO = websiteService.getDocDetail(id);
        DocInfoViewVO docInfoViewVO = CopyUtil.deepCopy(docInfoViewDTO, DocInfoViewVO.class);
        return Result.ok(docInfoViewVO);
    }

}
