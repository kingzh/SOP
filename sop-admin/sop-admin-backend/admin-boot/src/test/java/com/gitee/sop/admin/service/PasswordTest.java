package com.gitee.sop.admin.service;

import com.gitee.sop.admin.BaseTest;
import com.gitee.sop.admin.dao.entity.SysUser;
import com.gitee.sop.admin.service.sys.SysUserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;


/**
 * @author 六如
 */
public class PasswordTest extends BaseTest {

    @Autowired
    SysUserService sysAdminUserService;

    /**
     * 重置admin密码
     */
    @Test
    public void resetAdminPwd() {
        String username = "admin";
        String defPassword = "123456";
        defPassword = DigestUtils.sha256Hex(defPassword);
        String encodedPassword = BCrypt.hashpw(defPassword, BCrypt.gensalt());
        System.out.println("数据库保存:" + encodedPassword);
        sysAdminUserService.query()
                .eq(SysUser::getUsername, username)
                .set(SysUser::getPassword, encodedPassword)
                .update();
    }

}
