package com.gitee.sop.admin.dao.mapper;

import com.gitee.fastmybatis.core.mapper.BaseMapper;
import com.gitee.sop.admin.dao.entity.IsvKeys;

/**
 * @author 六如
 */
public interface IsvKeysMapper extends BaseMapper<IsvKeys> {

    default IsvKeys getByIsvInfoId(Long isvId) {
        return this.get(IsvKeys::getIsvId, isvId);
    }

}
