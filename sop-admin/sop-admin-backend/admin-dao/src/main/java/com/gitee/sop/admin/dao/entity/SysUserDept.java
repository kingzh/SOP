package com.gitee.sop.admin.dao.entity;

import java.time.LocalDateTime;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;

import lombok.Data;


/**
 * 表名：sys_user_dept
 * 备注：用户部门关联表
 *
 * @author 六如
 */
@Table(name = "sys_user_dept", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
@Data
public class SysUserDept {

    /**
     * id
     */
    private Long id;

    /**
     * sys_user.id
     */
    private Long userId;

    /**
     * sys_dept.id
     */
    private Long deptId;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 修改人id
     */
    private Long updateBy;


}
