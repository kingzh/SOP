package com.gitee.sop.admin.dao.entity;

import java.time.LocalDateTime;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;

import lombok.Data;


/**
 * 表名：doc_info
 * 备注：文档信息
 *
 * @author 六如
 */
@Table(name = "doc_info", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
@Data
public class DocInfo {

    /**
     * id
     */
    private Long id;

    /**
     * doc_app.id
     */
    private Long docAppId;

    /**
     * 远程文档id
     */
    private Long docId;

    /**
     * 文档标题
     */
    private String docTitle;

    /**
     * 文档code
     */
    private String docCode;

    /**
     * 文档类型,1-dubbo,2-富文本,3-Markdown
     */
    private Integer docType;

    /**
     * 来源类型,1-torna,2-自建
     */
    private Integer sourceType;

    /**
     * 文档名称
     */
    private String docName;

    /**
     * 版本号
     */
    private String docVersion;

    /**
     * 描述
     */
    private String description;

    /**
     * 是否分类
     */
    private Integer isFolder;

    /**
     * 状态, 0-未发布,1-已发布
     */
    private Integer isPublish;

    /**
     * 父节点id, 对应docId
     */
    private Long parentId;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 修改人id
     */
    private Long updateBy;


}
