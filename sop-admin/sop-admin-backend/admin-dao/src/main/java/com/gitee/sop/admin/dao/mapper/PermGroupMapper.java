package com.gitee.sop.admin.dao.mapper;

import com.gitee.fastmybatis.core.mapper.BaseMapper;
import com.gitee.sop.admin.dao.entity.PermGroup;

/**
 * @author 六如
 */
public interface PermGroupMapper extends BaseMapper<PermGroup> {

}
