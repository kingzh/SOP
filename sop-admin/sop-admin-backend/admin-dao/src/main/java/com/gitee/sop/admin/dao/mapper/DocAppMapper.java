package com.gitee.sop.admin.dao.mapper;

import com.gitee.fastmybatis.core.mapper.BaseMapper;
import com.gitee.sop.admin.dao.entity.DocApp;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 六如
 */
@Mapper
public interface DocAppMapper extends BaseMapper<DocApp> {
    default String getToken(Long id) {
        return this.query()
                .eq(DocApp::getId, id)
                .getValue(DocApp::getToken);
    }
}
