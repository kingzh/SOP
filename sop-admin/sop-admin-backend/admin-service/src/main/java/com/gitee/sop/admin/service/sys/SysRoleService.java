package com.gitee.sop.admin.service.sys;

import com.gitee.sop.admin.common.dto.StatusUpdateDTO;
import com.gitee.sop.admin.common.support.ServiceSupport;
import com.gitee.sop.admin.dao.entity.SysRole;
import com.gitee.sop.admin.dao.mapper.SysRoleMapper;
import org.springframework.stereotype.Service;


/**
 * @author 六如
 */
@Service
public class SysRoleService implements ServiceSupport<SysRole, SysRoleMapper> {


    /**
     * 修改状态
     *
     * @param statusUpdateDTO 修改值
     * @return 返回影响行数
     */
    public int updateStatus(StatusUpdateDTO statusUpdateDTO) {
        return this.query()
                .eq(SysRole::getId, statusUpdateDTO.getId())
                .set(SysRole::getStatus, statusUpdateDTO.getStatus())
                .update();
    }



}
