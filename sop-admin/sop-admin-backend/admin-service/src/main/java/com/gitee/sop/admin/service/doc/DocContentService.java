package com.gitee.sop.admin.service.doc;

import com.gitee.sop.admin.common.support.ServiceSupport;
import com.gitee.sop.admin.dao.entity.DocContent;
import com.gitee.sop.admin.dao.mapper.DocContentMapper;
import org.springframework.stereotype.Service;


/**
 * @author 六如
 */
@Service
public class DocContentService implements ServiceSupport<DocContent, DocContentMapper> {

    public void saveContent(Long docInfoId, String content) {
        DocContent docContent = this.get(DocContent::getDocInfoId, docInfoId);
        boolean save = false;
        if (docContent == null) {
            save = true;
            docContent = new DocContent();
        }
        docContent.setDocInfoId(docInfoId);
        docContent.setContent(content);
        if (save) {
            this.save(docContent);
        } else {
            this.update(docContent);
        }
    }

    public String getContent(Long docInfoId) {
        return this.query()
                .eq(DocContent::getDocInfoId, docInfoId)
                .getValueOptional(DocContent::getContent)
                .orElse("");
    }


}
