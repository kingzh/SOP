package com.gitee.sop.admin.service.sys;

import com.gitee.fastmybatis.core.util.TreeUtil;
import com.gitee.sop.admin.common.enums.MenuTypeEnum;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.dao.entity.SysResource;
import com.gitee.sop.admin.dao.entity.SysRoleResource;
import com.gitee.sop.admin.service.sys.dto.MenuTreeDTO;
import com.gitee.sop.admin.service.sys.dto.MenuTreeDTO.Meta;
import com.gitee.sop.admin.service.sys.dto.SysRoleDTO;
import com.gitee.sop.admin.service.sys.dto.UserPermissionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author 六如
 */
@Service
public class UserPermissionService {

    @Autowired
    private SysResourceService sysResourceService;
    @Autowired
    private SysRoleResourceService sysRoleResourceService;
    @Autowired
    private SysUserRoleService sysUserRoleService;

    public UserPermissionDTO getUserPermission(Long userId) {
        List<SysRoleDTO> sysRoleDTOS = sysUserRoleService.listUserRoles(userId);
        if (sysRoleDTOS.isEmpty()) {
            return UserPermissionDTO.empty();
        }

        Set<Long> roleIds = sysRoleDTOS.stream().map(SysRoleDTO::getId).collect(Collectors.toSet());
        List<String> roleCodes = sysRoleDTOS.stream().map(SysRoleDTO::getCode).distinct().collect(Collectors.toList());

        // 角色对应的资源
        List<Long> resourceIds = sysRoleResourceService.query()
                .in(SysRoleResource::getRoleId, roleIds)
                .listUniqueValue(SysRoleResource::getResourceId);

        // 获取按钮权限
        List<String> permissions = sysResourceService.query()
                .eq(SysResource::getMenuType, MenuTypeEnum.BUTTON.getValue())
                .in(SysResource::getId, resourceIds)
                .listUniqueValue(SysResource::getAuths);

        UserPermissionDTO userPermissionDTO = new UserPermissionDTO();
        userPermissionDTO.setRoles(roleCodes);
        userPermissionDTO.setPermissions(permissions);
        return userPermissionDTO;
    }


    /**
     * 获取用户菜单
     *
     * @param userId 用户id
     * @return 返回菜单树
     */
    public List<MenuTreeDTO> listUserMenuTree(Long userId) {
        List<Long> roleIds = sysUserRoleService.listUserRoleIds(userId);
        if (roleIds.isEmpty()) {
            return Collections.emptyList();
        }
        List<Long> resourceIds = sysRoleResourceService.listRoleResource(roleIds);
        if (resourceIds.isEmpty()) {
            return Collections.emptyList();
        }
        List<SysResource> list = sysResourceService.query()
                .in(SysResource::getId, resourceIds)
                .eq(SysResource::getMenuType, MenuTypeEnum.MENU)
                .list();

        return convert(list);


    }

    private List<MenuTreeDTO> convert(List<SysResource> list) {
        List<MenuTreeDTO> menuTreeDTOS = list.stream()
                .map(sysResource -> {
                    MenuTreeDTO menuTreeDTO = CopyUtil.copyBean(sysResource, MenuTreeDTO::new);
                    Meta meta = CopyUtil.copyBean(sysResource, Meta::new);
                    menuTreeDTO.setMeta(meta);
                    return menuTreeDTO;
                })
                .collect(Collectors.toList());

        return TreeUtil.convertTree(menuTreeDTOS, 0L);
    }

}
