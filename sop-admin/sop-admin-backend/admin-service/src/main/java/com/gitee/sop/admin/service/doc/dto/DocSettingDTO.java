package com.gitee.sop.admin.service.doc.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * @author 六如
 */
@Data
public class DocSettingDTO {

    @Length(max = 256)
    private String tornaServerAddr;
    @Length(max = 256)
    private String openProdUrl;
    @Length(max = 256)
    private String openSandboxUrl;


}
