package com.gitee.sop.admin.service.website;

import com.gitee.fastmybatis.core.util.TreeUtil;
import com.gitee.sop.admin.common.constants.YesOrNo;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.dao.entity.DocApp;
import com.gitee.sop.admin.dao.entity.DocInfo;
import com.gitee.sop.admin.service.doc.DocAppService;
import com.gitee.sop.admin.service.doc.DocInfoService;
import com.gitee.sop.admin.service.doc.DocSettingService;
import com.gitee.sop.admin.service.doc.dto.DocAppDTO;
import com.gitee.sop.admin.service.doc.dto.DocInfoConfigDTO;
import com.gitee.sop.admin.service.doc.dto.DocInfoTreeDTO;
import com.gitee.sop.admin.service.doc.dto.DocInfoViewDTO;
import com.gitee.sop.admin.service.doc.dto.DocSettingDTO;
import com.gitee.sop.admin.service.doc.dto.torna.TornaDocInfoViewDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 六如
 */
@Service
public class WebsiteService {

    @Autowired
    private DocAppService docAppService;
    @Autowired
    private DocInfoService docInfoService;
    @Autowired
    private DocSettingService docSettingService;

    public List<DocAppDTO> listDocApp() {
        List<DocApp> docApps = docAppService.list(DocApp::getIsPublish, YesOrNo.YES);
        return CopyUtil.copyList(docApps, DocAppDTO::new);
    }

    public List<DocInfoTreeDTO> listDocMenuTree(Long docAppId) {
        List<DocInfo> list = docInfoService.query()
                .eq(DocInfo::getDocAppId, docAppId)
                .eq(DocInfo::getIsPublish, YesOrNo.YES)
                .list();
        List<DocInfoTreeDTO> treeList = CopyUtil.copyList(list, DocInfoTreeDTO::new);
        return TreeUtil.convertTree(treeList, 0L);
    }

    public DocInfoViewDTO getDocDetail(Long id) {
        TornaDocInfoViewDTO tornaDocInfoViewDTO = docInfoService.getDocDetail(id);
        DocInfoConfigDTO docInfoConfigDTO = buildDocInfoConfig();

        DocInfoViewDTO docInfoViewDTO = new DocInfoViewDTO();
        docInfoViewDTO.setDocInfoView(tornaDocInfoViewDTO);
        docInfoViewDTO.setDocInfoConfig(docInfoConfigDTO);
        return docInfoViewDTO;
    }


    private DocInfoConfigDTO buildDocInfoConfig() {
        DocSettingDTO docSetting = docSettingService.getDocSetting();
        return CopyUtil.copyBean(docSetting, DocInfoConfigDTO::new);
    }
}
