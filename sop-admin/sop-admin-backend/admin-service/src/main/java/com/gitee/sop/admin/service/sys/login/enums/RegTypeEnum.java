package com.gitee.sop.admin.service.sys.login.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * @author 六如
 */
@AllArgsConstructor
@Getter
public enum RegTypeEnum {
    BACKEND("backend"),
    FORM("form"),
    OAUTH("oauth"),
    LDAP("ldap");

    public static RegTypeEnum of(String source) {
        for (RegTypeEnum value : RegTypeEnum.values()) {
            if (Objects.equals(source, value.value)) {
                return value;
            }
        }
        return RegTypeEnum.BACKEND;
    }

    private final String value;
}
