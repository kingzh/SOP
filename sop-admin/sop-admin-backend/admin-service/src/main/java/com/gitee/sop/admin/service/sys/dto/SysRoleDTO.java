package com.gitee.sop.admin.service.sys.dto;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class SysRoleDTO {
    /**
     * id
     */
    private Long id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色code
     */
    private String code;

    /**
     * 备注
     */
    private String remark;
}
