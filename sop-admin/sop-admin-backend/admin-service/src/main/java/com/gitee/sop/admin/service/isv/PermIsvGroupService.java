package com.gitee.sop.admin.service.isv;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.sop.admin.common.support.ServiceSupport;
import com.gitee.sop.admin.dao.entity.PermIsvGroup;
import com.gitee.sop.admin.dao.mapper.PermIsvGroupMapper;
import com.gitee.sop.admin.service.isv.dto.IsvGroupSettingDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author 六如
 */
@Service
public class PermIsvGroupService implements ServiceSupport<PermIsvGroup, PermIsvGroupMapper> {

    @Autowired
    private PermGroupService permGroupService;

    public PageInfo<PermIsvGroup> doPage(LambdaQuery<PermIsvGroup> query) {
        query.orderByDesc(PermIsvGroup::getId);
        PageInfo<PermIsvGroup> page = this.page(query);

        // 格式转换
        return page.convert(isvInfo -> {

            return isvInfo;
        });
    }

    public Map<Long, List<String>> getIsvGroupNameMap(Collection<Long> isvIds) {
        List<PermIsvGroup> list = this.list(PermIsvGroup::getIsvId, isvIds);
        if (list.isEmpty()) {
            return new HashMap<>();
        }
        Set<Long> groupIds = list.stream().map(PermIsvGroup::getGroupId).collect(Collectors.toSet());
        Map<Long, String> groupCodeMap = permGroupService.getCodeNameMap(groupIds);
        return this.query()
                .in(PermIsvGroup::getIsvId, isvIds)
                .group(PermIsvGroup::getIsvId,
                        permIsvGroup -> groupCodeMap.getOrDefault(permIsvGroup.getGroupId(), ""));
    }

    @Transactional(rollbackFor = Exception.class)
    public int updateIsvGroup(IsvGroupSettingDTO isvGroupSettingDTO) {
        // 先删除所有
        int i = this.deleteByColumn(PermIsvGroup::getIsvId, isvGroupSettingDTO.getIsvId());
        List<Long> groupIds = isvGroupSettingDTO.getGroupIds();
        if (CollectionUtils.isEmpty(groupIds)) {
            return i;
        }
        List<PermIsvGroup> saveList = groupIds
                .stream()
                .map(groupId -> {
                    PermIsvGroup permIsvGroup = new PermIsvGroup();
                    permIsvGroup.setIsvId(isvGroupSettingDTO.getIsvId());
                    permIsvGroup.setGroupId(groupId);
                    return permIsvGroup;
                }).collect(Collectors.toList());

        return this.saveBatch(saveList);
    }

    public List<Long> listIsvGroupId(Long isvId) {
        List<PermIsvGroup> list = this.list(PermIsvGroup::getIsvId, isvId);
        if (list.isEmpty()) {
            return new ArrayList<>(0);
        }
        return list.stream().map(PermIsvGroup::getGroupId).distinct().collect(Collectors.toList());
    }

    public boolean isUsed(Long groupId) {
        return this.query()
                .eq(PermIsvGroup::getGroupId, groupId)
                .getCount() > 0;
    }

}
