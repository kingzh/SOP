package com.gitee.sop.admin.service.sys.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tanghc
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SystemConfigDTO {
    private String configKey;

    private String configValue;

    private String remark;
}
