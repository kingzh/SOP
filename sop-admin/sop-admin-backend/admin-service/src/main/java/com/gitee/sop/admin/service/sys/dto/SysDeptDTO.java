package com.gitee.sop.admin.service.sys.dto;

import com.gitee.fastmybatis.core.support.TreeNode;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;


/**
 * 备注：部门表
 *
 * @author 六如
 */
@Data
public class SysDeptDTO implements TreeNode<SysDeptDTO, Long> {

    /**
     * id
     */
    private Long id;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态，1：启用，2：禁用
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 父级id
     */
    private Long parentId;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 修改人id
     */
    private Long updateBy;

    private List<SysDeptDTO> children;


    @Override
    public Long takeId() {
        return id;
    }

    @Override
    public Long takeParentId() {
        return parentId;
    }

}
