package com.gitee.sop.admin.service.sys.dto;

import lombok.Data;

import java.util.List;

/**
 * 用户角色信息
 * @author 六如
 */
@Data
public class UserRoleInfoDTO {

    private Long userId;

    private List<Long> roleIds;

    private List<String> roleCodes;

}
