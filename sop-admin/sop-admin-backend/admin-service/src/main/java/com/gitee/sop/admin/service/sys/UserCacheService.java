package com.gitee.sop.admin.service.sys;

import com.gitee.sop.admin.common.user.User;

import java.util.Optional;

/**
 * @author 六如
 */
public interface UserCacheService {

    Optional<User> getUser(Long userId);

}
