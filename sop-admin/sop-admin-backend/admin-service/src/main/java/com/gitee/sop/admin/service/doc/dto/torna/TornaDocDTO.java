package com.gitee.sop.admin.service.doc.dto.torna;

import lombok.Data;

import java.util.List;

/**
 * @author 六如
 */
@Data
public class TornaDocDTO {

    private List<TornaDocInfoDTO> docList;

}
