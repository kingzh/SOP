package com.gitee.sop.admin.service.sys.dto;

import com.gitee.sop.admin.common.user.User;
import lombok.Data;

import java.time.LocalDateTime;


/**
 *
 * @author 六如
 */
@Data
public class SysUserDTO implements User {

    /**
     * id
     */
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 邮箱
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 性别,0-未知,1-男,2-女
     */
    private Integer gender;

    /**
     * 状态，1：启用，2：禁用
     */
    private Integer status;

    /**
     * 注册类型，1-系统，2-手动
     */
    private String regType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 修改人id
     */
    private Long updateBy;

    /**
     * 部门
     */
    private SysDeptDTO dept;

    @Override
    public Long getUserId() {
        return id;
    }

    @Override
    public String getToken() {
        return "";
    }

    public String getShowName() {
        return nickname != null && !nickname.isEmpty() ? nickname : username;
    }
}
