package com.gitee.sop.admin.service.doc;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.gitee.httphelper.HttpHelper;
import com.gitee.sop.admin.common.enums.ConfigKeyEnum;
import com.gitee.sop.admin.common.exception.BizException;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author 六如
 */
@Component
public class TornaClient {

    public <T> T execute(String name, Object param, String token, Class<T> respClass) {
        JSONObject data = request(name, param, token).getJSONObject("data");
        return data.toJavaObject(respClass);
    }

    public <T> List<T> executeList(String name, Object param, String token, Class<T> respClass) {
        JSONArray data = request(name, param, token).getJSONArray("data");
        return data.toList(respClass);
    }

    private JSONObject request(String name, Object param, String token) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        params.put("access_token", token);
        if (param != null) {
            String json = JSON.toJSONString(param);
            params.put("data", UriUtils.encode(json, StandardCharsets.UTF_8));
        }
        try {
            String body = HttpHelper.postJson(getTornaApiUrl(), JSON.toJSONString(params))
                    .execute()
                    .asString();
            JSONObject jsonObject = JSON.parseObject(body);
            if (!Objects.equals("0", jsonObject.getString("code"))) {
                throw new BizException(jsonObject.getString("msg"));
            }
            return jsonObject;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getTornaApiUrl() {
        String value = ConfigKeyEnum.TORNA_SERVER_ADDR.getValue();
        if (ObjectUtils.isEmpty(value)) {
            throw new BizException("Torna服务器地址未配置");
        }
        return StringUtils.trimTrailingCharacter(value, '/') + "/api";
    }

}
