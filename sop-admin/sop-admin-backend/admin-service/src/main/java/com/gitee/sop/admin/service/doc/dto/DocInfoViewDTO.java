package com.gitee.sop.admin.service.doc.dto;

import com.gitee.sop.admin.service.doc.dto.torna.TornaDocInfoViewDTO;
import lombok.Data;

/**
 * @author 六如
 */
@Data
public class DocInfoViewDTO {

    private TornaDocInfoViewDTO docInfoView;

    private DocInfoConfigDTO docInfoConfig;

}
