package com.gitee.sop.admin.service.sys.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author 六如
 */
@Data
@AllArgsConstructor
public class DeptUserResultDTO {

    private Boolean select = true;

    private List<Long> userIds;

}
