package com.gitee.sop.admin.service.isv;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.sop.admin.common.context.SpringContext;
import com.gitee.sop.admin.common.support.ServiceSupport;
import com.gitee.sop.admin.dao.entity.ApiInfo;
import com.gitee.sop.admin.dao.entity.PermGroupPermission;
import com.gitee.sop.admin.dao.entity.PermIsvGroup;
import com.gitee.sop.admin.dao.mapper.ApiInfoMapper;
import com.gitee.sop.admin.dao.mapper.PermGroupPermissionMapper;
import com.gitee.sop.admin.dao.mapper.PermIsvGroupMapper;
import com.gitee.sop.admin.service.isv.dto.PermGroupPermissionDTO;
import com.gitee.sop.admin.service.isv.event.ChangeIsvPermEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author 六如
 */
@Service
public class PermGroupPermissionService implements ServiceSupport<PermGroupPermission, PermGroupPermissionMapper> {

    @Autowired
    private ApiInfoMapper apiInfoMapper;
    @Autowired
    private PermIsvGroupMapper permIsvGroupMapper;

    public PageInfo<PermGroupPermission> doPage(LambdaQuery<PermGroupPermission> query) {
        query.orderByDesc(PermGroupPermission::getId);
        PageInfo<PermGroupPermission> page = this.page(query);

        // 格式转换
        return page.convert(isvInfo -> {

            return isvInfo;
        });
    }

    public PageInfo<ApiInfo> pageGroupApiId(Long groupId, LambdaQuery<ApiInfo> query) {
        List<Long> apiIds = this.query()
                .eq(PermGroupPermission::getGroupId, groupId)
                .listUniqueValue(PermGroupPermission::getApiId);

        if (apiIds.isEmpty()) {
            return new PageInfo<>();
        }

        query.in(ApiInfo::getId, apiIds);

        return apiInfoMapper.page(query);
    }

    /**
     * 设置组权限
     *
     * @param param
     * @return
     */
    public int setting(PermGroupPermissionDTO param) {
        Long groupId = param.getGroupId();
        List<Long> apiIdList = param.getApiIdList();
        if (CollectionUtils.isEmpty(apiIdList)) {
            return 0;
        }

        List<Long> existApiIdList = this.query()
                .eq(PermGroupPermission::getGroupId, groupId)
                .listUniqueValue(PermGroupPermission::getApiId);

        List<PermGroupPermission> saveList = apiIdList.stream()
                // 已存在的不添加
                .filter(apiId -> !existApiIdList.contains(apiId))
                .map(apiId -> {
                    PermGroupPermission permGroupPermission = new PermGroupPermission();
                    permGroupPermission.setGroupId(groupId);
                    permGroupPermission.setApiId(apiId);
                    return permGroupPermission;
                })
                .collect(Collectors.toList());
        int cnt = this.saveBatch(saveList);

        // 刷新isv权限
        this.refreshIsvPerm(groupId);

        return cnt;
    }

    private void refreshIsvPerm(Long groupId) {
        List<Long> isvIds = permIsvGroupMapper.query()
                .eq(PermIsvGroup::getGroupId, groupId)
                .listUniqueValue(PermIsvGroup::getIsvId);

        SpringContext.publishEvent(new ChangeIsvPermEvent(isvIds));
    }


    public int delete(Long groupId, Collection<Long> apiIds) {
        int cnt = this.query()
                .eq(PermGroupPermission::getGroupId, groupId)
                .in(PermGroupPermission::getApiId, apiIds)
                .delete();

        this.refreshIsvPerm(groupId);

        return cnt;
    }
}
