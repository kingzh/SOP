package com.gitee.sop.admin.service.isv.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Collection;

/**
 * @author 六如
 */
@Getter
@AllArgsConstructor
public class ChangeIsvInfoEvent {

    private final Collection<Long> isvIds;

}
