package com.gitee.sop.admin.service.sys.login.dto;

import com.gitee.sop.admin.service.sys.login.enums.RegTypeEnum;
import lombok.Data;

/**
 * @author 六如
 */
@Data
public class LoginResult {

    private String username;

    private String nickname;

    private String email;

    private RegTypeEnum regTypeEnum = RegTypeEnum.FORM;

}
