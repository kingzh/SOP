package com.gitee.sop.admin.service.sys.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gitee.fastmybatis.core.support.TreeNode;
import com.gitee.sop.admin.common.jackson.convert.annotation.Bool;
import lombok.Data;

import java.util.List;


/**
 * 菜单资源
 *
 * @author 六如
 * @see <a href="https://pure-admin.cn/pages/routerMenu/#%E8%B7%AF%E7%94%B1%E5%92%8C%E8%8F%9C%E5%8D%95%E9%85%8D%E7%BD%AE">...</a>
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MenuTreeDTO implements TreeNode<MenuTreeDTO, Long> {

    /**
     * id
     */
    private Long id;

    /**
     * 路由路径
     */
    private String path;

    /**
     * 路由名称（必须保持唯一）
     */
    private String name;

    /**
     * 路由重定向（默认跳转地址）
     */
    private String redirect;

    /**
     * 父级id
     */
    private Long parentId;

    /**
     * 路由元信息
     */
    private Meta meta;

    private List<MenuTreeDTO> children;

    @Override
    public Long takeId() {
        return id;
    }

    @Override
    public Long takeParentId() {
        return parentId;
    }

    /**
     * 路由元信息，通俗来说就是路由上的额外信息 https://router.vuejs.org/zh/guide/advanced/meta.html#%E8%B7%AF%E7%94%B1%E5%85%83%E4%BF%A1%E6%81%AF
     */
    @Data
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class Meta {
        /**
         * 菜单名称（兼容国际化、非国际化，如果用国际化的写法就必须在根目录的locales文件夹下对应添加）
         */
        private String title;

        /**
         * 菜单图标
         */
        private String icon;

        /**
         * 右侧图标
         */
        private String extraIcon;

        /**
         * 菜单（是否显示该菜单）
         */
        @Bool
        private Integer showLink;

        /**
         * 父级菜单（是否显示父级菜单
         */
        @Bool
        private Integer showParent;

        /**
         * 链接地址（需要内嵌的`iframe`链接地址）
         */
        private String frameSrc;

        /**
         * 加载动画（内嵌的`iframe`页面是否开启首次加载动画）
         */
        @Bool
        private Integer frameLoading;

        /**
         * 缓存页面
         */
        @Bool
        private Integer keepAlive;

        /**
         * 标签页（当前菜单名称或自定义信息禁止添加到标签页）
         */
        @Bool
        private Integer hiddenTag;

        /**
         * 菜单激活
         */
        private String activePath;

        /**
         * 排序
         */
        private Integer rank;

    }
}
