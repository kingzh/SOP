package com.gitee.sop.admin.service.isv.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * @author 六如
 */
@Getter
@AllArgsConstructor
public class ChangeIsvPermEvent {

    private final List<Long> isvIds;

}
