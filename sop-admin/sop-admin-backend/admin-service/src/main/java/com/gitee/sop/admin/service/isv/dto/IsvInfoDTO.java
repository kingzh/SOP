package com.gitee.sop.admin.service.isv.dto;

import com.gitee.sop.admin.service.jackson.convert.annotation.UserFormat;
import lombok.Data;

import java.time.LocalDateTime;


/**
 *
 * @author 六如
 */
@Data
public class IsvInfoDTO {

    private Long id;

    /**
     * appKey
     */
    private String appId;

    /**
     * 1启用，2禁用
     */
    private Integer status;

    /**
     * 是否有秘钥
     */
    private Integer hasKeys;

    /**
     * 备注
     */
    private String remark;

    private String groupNames;

    private LocalDateTime addTime;

    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    @UserFormat
    private Long addBy;

    /**
     * 修改人id
     */
    @UserFormat
    private Long updateBy;

}
