package com.gitee.sop.admin.service.sys.login;

import com.gitee.sop.admin.service.sys.login.dto.LoginForm;
import com.gitee.sop.admin.service.sys.login.dto.LoginResult;

/**
 * @author 六如
 */
public interface ThirdPartyLoginManager {

    /**
     * 第三方登录
     * @param loginForm 登录表单
     * @return 返回登录结果
     */
    LoginResult login(LoginForm loginForm) throws Exception;
}
