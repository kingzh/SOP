package com.gitee.sop.admin.service.sys.dto;

import lombok.Data;

import java.util.List;

/**
 * @author 六如
 */
@Data
public class SysRoleResourceDTO {

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 资源id
     */
    private List<Long> resourceIds;

    /**
     * 添加人
     */
    private Long addBy;

}
