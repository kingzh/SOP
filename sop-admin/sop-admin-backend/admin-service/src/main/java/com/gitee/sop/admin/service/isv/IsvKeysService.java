package com.gitee.sop.admin.service.isv;

import com.gitee.sop.admin.common.context.SpringContext;
import com.gitee.sop.admin.common.support.ServiceSupport;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.dao.entity.IsvKeys;
import com.gitee.sop.admin.dao.mapper.IsvKeysMapper;
import com.gitee.sop.admin.service.isv.dto.IsvInfoUpdateKeysDTO;
import com.gitee.sop.admin.service.isv.event.ChangeIsvKeyEvent;
import org.springframework.stereotype.Service;

import java.util.Collections;


/**
 * @author 六如
 */
@Service
public class IsvKeysService implements ServiceSupport<IsvKeys, IsvKeysMapper> {

    public int saveKeys(IsvInfoUpdateKeysDTO isvInfoUpdateKeysDTO) {
        IsvKeys isvKeys = this.get(IsvKeys::getIsvId, isvInfoUpdateKeysDTO.getIsvId());
        if (isvKeys == null) {
            isvKeys = new IsvKeys();
        }
        CopyUtil.copyPropertiesIgnoreNull(isvInfoUpdateKeysDTO, isvKeys);
        int cnt = this.saveOrUpdate(isvKeys);
        // 发送变更事件
        SpringContext.publishEvent(new ChangeIsvKeyEvent(Collections.singletonList(isvInfoUpdateKeysDTO.getIsvId())));
        return cnt;
    }

}
