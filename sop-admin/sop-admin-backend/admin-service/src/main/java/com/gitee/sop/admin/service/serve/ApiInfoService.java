package com.gitee.sop.admin.service.serve;

import com.gitee.sop.admin.common.context.SpringContext;
import com.gitee.sop.admin.common.dto.StatusUpdateDTO;
import com.gitee.sop.admin.common.support.ServiceSupport;
import com.gitee.sop.admin.dao.entity.ApiInfo;
import com.gitee.sop.admin.dao.mapper.ApiInfoMapper;
import com.gitee.sop.admin.service.isv.event.ChangeApiInfoEvent;
import org.springframework.stereotype.Service;

import java.util.Collections;


/**
 * @author 六如
 */
@Service
public class ApiInfoService implements ServiceSupport<ApiInfo, ApiInfoMapper> {

    /**
     * 修改状态
     *
     * @param statusUpdateDTO 修改值
     * @return 返回影响行数
     */
    public int updateStatus(StatusUpdateDTO statusUpdateDTO) {
        int cnt = this.query()
                .eq(ApiInfo::getId, statusUpdateDTO.getId())
                .set(ApiInfo::getStatus, statusUpdateDTO.getStatus())
                .update();
        this.sendChangeEvent(statusUpdateDTO.getId());
        return cnt;
    }

    @Override
    public int update(ApiInfo entity) {
        int cnt = ServiceSupport.super.update(entity);
        sendChangeEvent(entity.getId());
        return cnt;
    }



    private void sendChangeEvent(Long id) {
        SpringContext.publishEvent(new ChangeApiInfoEvent(Collections.singletonList(id)));
    }
}
