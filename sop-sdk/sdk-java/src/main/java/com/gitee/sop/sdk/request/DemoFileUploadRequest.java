package com.gitee.sop.sdk.request;

import com.gitee.sop.sdk.response.GetStoryResponse;

/**
 * @author 六如
 */
public class DemoFileUploadRequest extends BaseRequest<GetStoryResponse> {
    @Override
    protected String method() {
        return "story.upload.more";
    }
}
