package com.gitee.sop.sdk.model;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class DemoFileUploadModel {
    private String storyName;
    private String addTime;
}
