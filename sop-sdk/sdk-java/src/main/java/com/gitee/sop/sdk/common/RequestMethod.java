package com.gitee.sop.sdk.common;

/**
 * 请求方法枚举
 * @author 六如
 */
public enum RequestMethod {

	GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE

}
