package com.gitee.sop.sdk.common;

import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.sop.sdk.sign.StringUtils;
import lombok.Data;

/**
 * @author 六如
 */
@Data
public class Result<T> {
    private String code;
    private String msg;
    private String subCode;
    private String subMsg;
    private T data;

    @JSONField(serialize = false)
    public boolean isSuccess() {
        return StringUtils.isEmpty(subCode);
    }

}
