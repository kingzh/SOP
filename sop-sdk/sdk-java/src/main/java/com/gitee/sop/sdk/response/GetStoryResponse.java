package com.gitee.sop.sdk.response;

import lombok.Data;

import java.util.Date;

@Data
public class GetStoryResponse {
    private Long id;
    private String name;
    private Date addTime;
}
