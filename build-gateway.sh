# 构建网关


# 获取当前路径并赋值给变量 current_path
current_path=$(pwd)

# 打印变量的值，以验证赋值是否成功
echo "当前路径是: $current_path"

# 构建目录
dist_dir="dist"
# 服务端文件夹名称
# 执行文件名称
app_name="sop-gateway"
version="5.0"
build_folder="${app_name}-${version}"
# 输出目录
target_dir="$dist_dir/${build_folder}"

server_source=sop-gateway

# ------ 构建后端 ------
echo "开始构建sop-gateway..."

mvn clean package -pl $server_source -am -DskipTests

# ------ 复制文件 ------
if [ ! -d "$target_dir" ]; then
  mkdir -p $target_dir
fi

rm -rf ${target_dir}/*

# 复制服务端资源
cp -r ${server_source}/target/*.jar $target_dir
cp -r script/* $target_dir
cp -r ${server_source}/src/main/resources/application-test.properties $target_dir/application.properties

echo "服务端构建完毕，构建结果在${target_dir}文件夹下"
