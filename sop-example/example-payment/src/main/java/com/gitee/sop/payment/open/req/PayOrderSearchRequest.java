package com.gitee.sop.payment.open.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * @author 六如
 */
@Data
public class PayOrderSearchRequest {

    @ApiModelProperty(value = "订单编号", required = true, example = "xxxx")
    @Length(max = 64) // 最大长度
    private String orderNo;

}
