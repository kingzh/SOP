package com.gitee.sop.payment.open.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 六如
 */
@Data
public class PayOrderSearchResponse {

    @ApiModelProperty(value = "订单编号", example = "xxxx")
    private String orderNo;

    @ApiModelProperty(value = "支付编号", example = "xxxx")
    private String payNo;

    @ApiModelProperty(value = "支付人id", example = "111")
    private Long payUserId;

    @ApiModelProperty(value = "支付人姓名", example = "Jim")
    private String payUserName;
}
