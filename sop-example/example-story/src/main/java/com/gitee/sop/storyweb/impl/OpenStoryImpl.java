package com.gitee.sop.storyweb.impl;

import com.gitee.sop.storyweb.message.StoryMessageEnum;
import com.gitee.sop.storyweb.open.OpenStory;
import com.gitee.sop.storyweb.open.req.StorySaveRequest;
import com.gitee.sop.storyweb.open.resp.StoryResponse;
import com.gitee.sop.support.context.OpenContext;
import com.gitee.sop.support.dto.CommonFileData;
import com.gitee.sop.support.dto.FileData;
import com.gitee.sop.support.exception.OpenException;
import org.apache.commons.io.IOUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * 开放接口实现
 *
 * @author 六如
 */
@DubboService(validation = "true")
public class OpenStoryImpl implements OpenStory {


    @Override
    public Integer save(StorySaveRequest storySaveDTO) {
        return 1;
    }


    @Override
    public Integer update(Integer id, StorySaveRequest storySaveDTO) {
        System.out.println("update, id:" + id + ", storySaveDTO=" + storySaveDTO);
        return 1;
    }

    @Override
    public Integer updateError(Integer id) {
        // 抛出业务异常
        if (id == null || id == 0) {
            throw new OpenException(StoryMessageEnum.ISV_PARAM_ERROR, "id is null or 0");
        }
        if (id < 0) {
            // 自定义code, msg
            throw new OpenException("4000", "id必须大于0");
        }
        return 1;
    }

    @Override
    public StoryResponse getById(Integer id) {
        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setId(id);
        storyResponse.setName("乌鸦喝水");
        return storyResponse;
    }


    @Override
    public StoryResponse getByIdV2(Long id) {
        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setId(2);
        storyResponse.setName("乌鸦喝水2.0");
        return storyResponse;
    }

    // 演示获取上下文
    @Override
    public StoryResponse getContext(Long id, OpenContext context) {
        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setId(3);
        storyResponse.setName(context.toString());
        // 获取回调参数
        String notifyUrl = context.getNotifyUrl();
        System.out.println(notifyUrl);

        return storyResponse;
    }

    @Override
    public StoryResponse upload(StorySaveRequest storySaveDTO, FileData file) {
        System.out.println("getName:" + file.getName());
        System.out.println("getOriginalFilename:" + file.getOriginalFilename());
        checkFile(Arrays.asList(file));

        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setId(1);
        storyResponse.setName(file.getOriginalFilename());
        return storyResponse;
    }


    @Override
    public StoryResponse upload2(StorySaveRequest storySaveDTO, FileData idCardFront, FileData idCardBack) {
        System.out.println("upload:" + storySaveDTO);
        checkFile(Arrays.asList(idCardFront, idCardBack));

        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setId(1);
        storyResponse.setName(storySaveDTO.getStoryName());
        return storyResponse;
    }

    @Override
    public StoryResponse upload3(StorySaveRequest storySaveDTO, List<FileData> files) {
        List<String> list = new ArrayList<>();
        list.add("upload:" + storySaveDTO);
        checkFile(files);

        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setId(1);
        storyResponse.setName(storySaveDTO.getStoryName());
        return storyResponse;
    }

    @Override
    public FileData download(Integer id) {
        CommonFileData fileData = new CommonFileData();
        ClassPathResource resource = new ClassPathResource("download.txt");
        fileData.setOriginalFilename(resource.getFilename());
        try {
            fileData.setData(IOUtils.toByteArray(resource.getInputStream()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return fileData;
    }

    private void checkFile(List<FileData> fileDataList) {
        for (FileData file : fileDataList) {
            Assert.notNull(file.getName());
            Assert.notNull(file.getOriginalFilename());
            Assert.notNull(file.getBytes());
            Assert.isTrue(!file.isEmpty());
        }
    }
}
