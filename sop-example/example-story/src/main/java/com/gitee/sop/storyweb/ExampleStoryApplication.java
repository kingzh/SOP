package com.gitee.sop.storyweb;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
public class ExampleStoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExampleStoryApplication.class, args);
    }

}

