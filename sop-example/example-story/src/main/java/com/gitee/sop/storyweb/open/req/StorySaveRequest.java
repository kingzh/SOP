package com.gitee.sop.storyweb.open.req;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.context.annotation.Lazy;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author 六如
 */
@Data
public class StorySaveRequest implements Serializable {
    private static final long serialVersionUID = -1214422742659231037L;

    /**
     * 故事名称
     */
    @NotBlank(message = "故事名称必填")
    @Length(max = 64)
    private String storyName;

    /**
     * 添加时间
     */
    @NotNull(message = "添加时间必填")
    private Date addTime;

}
