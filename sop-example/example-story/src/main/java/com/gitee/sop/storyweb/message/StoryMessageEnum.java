package com.gitee.sop.storyweb.message;

import com.gitee.sop.support.message.I18nMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author 六如
 */
@AllArgsConstructor
@Getter
public enum StoryMessageEnum implements I18nMessage {
    /**
     * 参数错误
     */
    ISV_PARAM_ERROR("isv.invalid-parameter");

    private final String configKey;

}
