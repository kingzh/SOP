package com.gitee.sop.storyweb.open;

import com.gitee.sop.storyweb.open.req.StorySaveRequest;
import com.gitee.sop.storyweb.open.resp.StoryResponse;
import com.gitee.sop.support.annotation.Open;
import com.gitee.sop.support.context.OpenContext;
import com.gitee.sop.support.dto.FileData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 故事服务
 *
 * @author 六如
 * @dubbo
 */
@Api("故事服务")
public interface OpenStory {

    /**
     * 新增故事
     *
     * @param storySaveRequest 入参
     * @return 返回id
     */
    @Open("story.save")
    Integer save(StorySaveRequest storySaveRequest);

    @Open("story.update")
    Integer update(Integer id, StorySaveRequest storySaveRequest);

    // 演示抛出异常
    @Open("story.updateError")
    Integer updateError(Integer id);

    @ApiOperation(value = "根据id获取故事")
    @Open("story.get")
    StoryResponse getById(@NotNull(message = "id必填") Integer id);

    // 需要授权
    @Open(value = "story.get", version = "2.0", permission = true)
    StoryResponse getByIdV2(Long id);

    @Open(value = "story.get.context")
    StoryResponse getContext(Long id, OpenContext context);


    // 默认方法,注解放在这里也有效
    @Open("story.find")
    default StoryResponse getById(Integer id, String name) {
        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setId(id);
        storyResponse.setName(name);
        return storyResponse;
    }

    // 默认方法,注解放在这里也有效
    @Open("alipay.story.find")
    default StoryResponse findByName(String name) {
        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setName(name);
        return storyResponse;
    }

    // 演示单文件上传
    @Open("story.upload")
    StoryResponse upload(StorySaveRequest storySaveRequest, FileData file);

    // 演示多文件上传
    @Open("story.upload.more")
    StoryResponse upload2(
            StorySaveRequest storySaveRequest,
            @NotNull(message = "身份证正面必填") FileData idCardFront,
            @NotNull(message = "身份证背面必填") FileData idCardBack
    );

    // 演示多文件上传
    @Open("story.upload.list")
    StoryResponse upload3(
            StorySaveRequest storySaveRequest,
            @Size(min = 2, message = "最少上传2个文件")
            List<FileData> files
    );

    // 下载
    @Open("story.download")
    FileData download(Integer id);
}
