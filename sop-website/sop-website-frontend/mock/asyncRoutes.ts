// 模拟后端动态生成路由
import { defineFakeRoute } from "vite-plugin-fake-server/client";

/**
 * roles：页面级别权限，这里模拟二种 "admin"、"common"
 * admin：管理员角色
 * common：普通角色
 */
const permissionRouter = {
  path: "/doc",
  meta: {
    title: "API文档",
    icon: "ep:document",
    rank: 10
  },
  children: [
    {
      path: "/doc/api",
      name: "DocApi",
      meta: {
        title: "API文档"
      }
    },
    {
      path: "/doc/sign",
      name: "DocSign",
      meta: {
        title: "签名算法",
        showLink: false
      }
    },
    {
      path: "/doc/code",
      name: "DocCode",
      meta: {
        title: "公共错误码",
        showLink: false
      }
    }
  ]
};

export default defineFakeRoute([
  {
    url: "/get-async-routes",
    method: "get",
    response: () => {
      return {
        success: true,
        data: [permissionRouter]
      };
    }
  }
]);
