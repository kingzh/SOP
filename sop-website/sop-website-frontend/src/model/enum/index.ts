export enum DocType {
  DOC = 1,
  RICH_TEXT = 2,
  MARKDOWN = 3
}
