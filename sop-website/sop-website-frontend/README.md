# 接口文档预览

> 采用 ECMAScript 模块（ESM）规范来编写和组织代码，使用了最新的 Vue3、Vite、Element-Plus、TypeScript、Pinia、Tailwindcss 等主流技术

## 参考文档

- 模板:https://pure-admin.github.io/pure-admin-doc/
- 表单表格组件:https://plus-pro-components.com/

## 开发部署

安装nodejs

> node版本 >= 18.18.0

- macOS用户在命令前面添加`sudo`

```shell
# 安装pnpm
npm install -g pnpm

# 安装依赖
pnpm install

# 启动
pnpm dev
```

打包:

`pnpm build`

安装一个包:

`pnpm add 包名`

卸载一个包:

`pnpm remove 包名`