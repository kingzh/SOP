package com.gitee.sop.website.common.config;


import com.gitee.sop.website.common.context.SpringContext;
import com.gitee.sop.website.common.enums.ConfigKeyEnum;

import java.util.function.Supplier;

/**
 * @author 六如
 */
public class Configs {

    /**
     * 获取配置参数
     *
     * @param keyGetter 配置key
     * @return 返回配参数，没有则返回null
     */
    public static String getValue(ConfigKeyEnum keyGetter) {
        return getValue(keyGetter, keyGetter.getDefaultValue());
    }

    /**
     * 获取配置参数
     *
     * @param keyGetter    配置key
     * @param defaultValue 默认值
     * @return 返回配参数，没有则返回默认值
     */
    public static String getValue(ConfigKeyEnum keyGetter, String defaultValue) {
        return SpringContext.getBean(IConfig.class).getConfig(keyGetter.getKey(), defaultValue);
    }

    /**
     * 获取配置参数
     *
     * @param keyGetter    配置key
     * @param defaultValue 默认值
     * @return 返回配参数，没有则返回默认值
     */
    public static String getValue(ConfigKeyEnum keyGetter, Supplier<String> defaultValue) {
        return getValue(keyGetter, defaultValue.get());
    }

}
