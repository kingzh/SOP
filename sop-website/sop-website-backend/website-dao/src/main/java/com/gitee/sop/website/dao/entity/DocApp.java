package com.gitee.sop.website.dao.entity;

import java.time.LocalDateTime;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;

import lombok.Data;


/**
 * 表名：doc_app
 * 备注：文档应用
 *
 * @author 六如
 */
@Table(name = "doc_app", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
@Data
public class DocApp {

    /**
     * id
     */
    private Long id;

    /**
     * 应用名称
     */
    private String appName;

    /**
     * Torna应用token
     */
    private String token;

    /**
     * 状态, 0-未发布,1-已发布
     */
    private Integer isPublish;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 修改人id
     */
    private Long updateBy;


}
