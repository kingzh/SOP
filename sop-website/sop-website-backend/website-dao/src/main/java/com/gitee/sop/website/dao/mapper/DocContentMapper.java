package com.gitee.sop.website.dao.mapper;

import com.gitee.fastmybatis.core.mapper.BaseMapper;
import com.gitee.sop.website.dao.entity.DocContent;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 六如
 */
@Mapper
public interface DocContentMapper extends BaseMapper<DocContent> {

}
