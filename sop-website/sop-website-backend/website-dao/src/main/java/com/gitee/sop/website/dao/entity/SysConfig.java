package com.gitee.sop.website.dao.entity;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * 表名：sys_config
 * 备注：系统配置表
 *
 * @author 六如
 */
@Table(name = "sys_config", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
@Data
public class SysConfig {

    private Long id;

    private String configKey;

    private String configValue;

    private String remark;

    @com.gitee.fastmybatis.annotation.Column(logicDelete = true)
    private Integer isDeleted;

    private LocalDateTime addTime;

    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 最后更新人id
     */
    private Long updateBy;
}
