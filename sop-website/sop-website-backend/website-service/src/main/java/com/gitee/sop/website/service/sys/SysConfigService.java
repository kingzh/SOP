package com.gitee.sop.website.service.sys;

import com.gitee.fastmybatis.core.support.BaseLambdaService;
import com.gitee.sop.website.common.config.IConfig;
import com.gitee.sop.website.dao.entity.SysConfig;
import com.gitee.sop.website.dao.mapper.SysConfigMapper;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author 六如
 */
@Service
public class SysConfigService extends BaseLambdaService<SysConfig, SysConfigMapper> implements IConfig {

    @Autowired
    private Environment environment;

    // key: configKey, value: configValue
    private final LoadingCache<String, Optional<String>> configCache = CacheBuilder.newBuilder()
            .expireAfterAccess(15, TimeUnit.MINUTES)
            .build(new CacheLoader<String, Optional<String>>() {
                @Override
                public Optional<String> load(String key) throws Exception {
                    return Optional.ofNullable(getConfigValue(key, null));
                }
            });


    /**
     * 获取配置信息
     * <pre>
     *  优先级：
     *  数据库
     *  Environment
     *  默认配置
     * </pre>
     *
     * @param key          配置key
     * @param defaultValue 没有获取到返回的默认值
     * @return 返回配置信息，如果没有获取到值，则返回默认值
     */
    public String getConfigValue(String key, String defaultValue) {
        Objects.requireNonNull(key, "need key");
        SysConfig systemConfig = get(SysConfig::getConfigKey, key);
        return Optional.ofNullable(systemConfig)
                .map(SysConfig::getConfigValue)
                .orElseGet(() -> environment.getProperty(key, defaultValue));
    }

    @Override
    public String getConfig(String key) {
        return configCache.getUnchecked(key).orElse(null);
    }

    @Override
    public String getConfig(String key, String defaultValue) {
        return configCache.getUnchecked(key).orElse(defaultValue);
    }

}
