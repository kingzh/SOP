package com.gitee.sop.admin.service.doc.dto;

import lombok.Data;

import java.time.LocalDateTime;


/**
 * 备注：文档应用
 *
 * @author 六如
 */
@Data
public class DocAppDTO {

    /**
     * id
     */
    private Long id;

    /**
     * 应用名称
     */
    private String appName;

    /**
     * Torna应用token
     */
    private String token;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 修改人id
     */
    private Long updateBy;


}
