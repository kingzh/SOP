package com.gitee.sop.website.service.doc;

import com.gitee.sop.website.common.enums.ConfigKeyEnum;
import com.gitee.sop.website.service.doc.dto.DocSettingDTO;
import org.springframework.stereotype.Service;


/**
 * @author 六如
 */
@Service
public class DocSettingService {

    public DocSettingDTO getDocSetting() {
        DocSettingDTO docSettingDTO = new DocSettingDTO();
        docSettingDTO.setTornaServerAddr(ConfigKeyEnum.TORNA_SERVER_ADDR.getValue());
        docSettingDTO.setOpenProdUrl(ConfigKeyEnum.OPEN_PROD_URL.getValue());
        docSettingDTO.setOpenSandboxUrl(ConfigKeyEnum.OPEN_SANDBOX_URL.getValue());
        return docSettingDTO;
    }

}
