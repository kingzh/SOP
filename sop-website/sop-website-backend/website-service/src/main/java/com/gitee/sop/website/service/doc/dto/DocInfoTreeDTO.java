package com.gitee.sop.website.service.doc.dto;

import com.gitee.fastmybatis.core.support.TreeNode;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;


/**
 * 备注：文档信息
 *
 * @author 六如
 */
@Data
public class DocInfoTreeDTO implements TreeNode<DocInfoTreeDTO, Long> {

    /**
     * id
     */
    private Long id;

    /**
     * doc_app.id
     */
    private Long docAppId;

    /**
     * 文档id
     */
    private Long docId;

    /**
     * 文档标题
     */
    private String docTitle;

    /**
     * 文档code
     */
    private String docCode;

    /**
     * 文档类型,1-dubbo,2-富文本,3-Markdown
     */
    private Integer docType;

    /**
     * 来源类型,1-torna,2-自建
     */
    private Integer sourceType;

    /**
     * 文档名称
     */
    private String docName;

    /**
     * 版本号
     */
    private String docVersion;

    /**
     * 描述
     */
    private String description;

    /**
     * 是否分类
     */
    private Integer isFolder;

    /**
     * 状态, 0-未发布,1-已发布
     */
    private Integer isPublish;

    /**
     * 父节点id
     */
    private Long parentId;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    private List<DocInfoTreeDTO> children;


    @Override
    public Long takeId() {
        return docId;
    }

    @Override
    public Long takeParentId() {
        return parentId;
    }

}
