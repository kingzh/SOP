package com.gitee.sop.website;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SopWebsiteApplication {

    public static void main(String[] args) {
        SpringApplication.run(SopWebsiteApplication.class, args);
    }

}
