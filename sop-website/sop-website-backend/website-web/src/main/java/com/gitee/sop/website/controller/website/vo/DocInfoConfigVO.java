package com.gitee.sop.website.controller.website.vo;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class DocInfoConfigVO {

    private String openProdUrl;
    private String openSandboxUrl;

}
