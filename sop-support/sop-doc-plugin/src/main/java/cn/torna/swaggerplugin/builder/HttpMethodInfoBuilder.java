package cn.torna.swaggerplugin.builder;

import cn.torna.swaggerplugin.bean.ControllerInfo;
import cn.torna.swaggerplugin.bean.TornaConfig;
import io.swagger.annotations.ApiOperation;

import java.lang.reflect.Method;

/**
 * @author tanghc
 */
public abstract class HttpMethodInfoBuilder implements RequestInfoBuilder {

    private ControllerInfo controllerInfo;
    private TornaConfig tornaConfig;
    private Method method;
    private ApiOperation apiOperation;

    public HttpMethodInfoBuilder(ControllerInfo controllerInfo, Method method, TornaConfig tornaConfig) {
        this.controllerInfo = controllerInfo;
        this.tornaConfig = tornaConfig;
        this.method = method;
        this.apiOperation = method.getAnnotation(ApiOperation.class);
    }

    @Override
    public String getHttpMethod() {
        return tornaConfig.getDefaultHttpMethod();
    }

    public TornaConfig getTornaConfig() {
        return tornaConfig;
    }

    @Override
    public Method getMethod() {
        return method;
    }

    @Override
    public ApiOperation getApiOperation() {
        return apiOperation;
    }

    public ControllerInfo getControllerInfo() {
        return controllerInfo;
    }
}
