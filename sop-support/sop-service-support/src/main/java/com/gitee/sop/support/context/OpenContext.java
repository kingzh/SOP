package com.gitee.sop.support.context;

import java.util.Locale;

/**
 * @author 六如
 */
public interface OpenContext {

    /**
     * 获取appId
     */
    String getAppId();

    /**
     * 获取apiName
     */
    String getApiName();

    /**
     * 获取version
     */
    String getVersion();

    /**
     * 获取token,没有返回null
     */
    String getAppAuthToken();

    /**
     * 获取客户端ip
     */
    String getClientIp();

    /**
     * 获取回调地址
     */
    String getNotifyUrl();

    /**
     * 获取唯一请求id
     */
    String getTraceId();

    /**
     * 获取locale
     */
    Locale getLocale();

}
