package com.gitee.sop.support.doc;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class OpenAnnotationInfo {

    /**
     * 接口名
     */
    private String value;

    /**
     * 版本号
     */
    private String version;


}
